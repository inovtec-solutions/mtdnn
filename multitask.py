#!/usr/bin/python

from singletask import SingleTaskDNN
from lasagne.layers import get_all_param_values, set_all_param_values
from os.path import exists
import theano.tensor as tensor
import theano
from sklearn.metrics import f1_score, confusion_matrix
import logging
import utils
import pandas as pd
import numpy as np
import pickle
import copy
import random
from numpy import inf
import os

def xrange(x):
    return iter(range(x))

class DataSet(object):
    def __init__(self, data_array, labels):
        assert data_array.shape[0] == labels.shape[0], (
            'images.shape: %s labels.shape: %s' % (data_array.shape,
                                                   labels.shape))

        self._num_examples = data_array.shape[0]
        data_array = data_array.astype(np.float32)
        self._data = data_array
        self._data_pos = data_array[labels.ravel() == 1, :]
        self._data_neg = data_array[labels.ravel() == 0, :]
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch_pos = 0
        self._index_in_epoch_neg = 0
        self._num_pos_examples = self._data_pos.shape[0]
        self._num_neg_examples = self._data_neg.shape[0]

    def shuffle(self):
        index = np.arange(self._num_examples)
        np.random.shuffle(index)
        data_array = self._data[index, :]
        labels = self._labels[index]
        self._data = data_array
        self._labels = labels
        self._data_pos = data_array[labels.ravel() == 1, :]
        self._data_neg = data_array[labels.ravel() == 0, :]

    def data_array(self):
        return self._data

    def labels(self):
        return self._labels

    def num_examples(self):
        return self._num_examples

    def epochs_completed(self):
        return self._epochs_completed

    def refresh(self):
        self._epochs_completed = 0
        self._index_in_epoch_pos = 0
        self._index_in_epoch_neg = 0

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        start_pos = copy.deepcopy(self._index_in_epoch_pos)
        start_neg = copy.deepcopy(self._index_in_epoch_neg)
        self._index_in_epoch_pos += batch_size / 2
        self._index_in_epoch_neg += batch_size / 2
        if self._num_neg_examples > self._num_pos_examples:
            if self._index_in_epoch_neg > self._num_neg_examples:
                # Finished epoch
                self._epochs_completed += 1
                # Start next epoch
                start_neg = 0
                if (batch_size / 2) <= self._num_neg_examples:
                    self._index_in_epoch_neg = batch_size / 2
                else:
                    self._index_in_epoch_neg = self._num_neg_examples
            end_neg = copy.deepcopy(self._index_in_epoch_neg)
            if self._index_in_epoch_pos > self._num_pos_examples:
                # Start next epoch
                start_pos = 0
                self._index_in_epoch_pos = batch_size / 2
                if (batch_size / 2) <= self._num_pos_examples:
                    self._index_in_epoch_pos = batch_size / 2
                else:
                    self._index_in_epoch_pos = self._num_pos_examples
            end_pos = copy.deepcopy(self._index_in_epoch_pos)
        else:
            if self._index_in_epoch_neg > self._num_neg_examples:
                # Start next epoch
                start_neg = 0
                self._index_in_epoch_neg = batch_size / 2
                if (batch_size / 2) <= self._num_neg_examples:
                    self._index_in_epoch_neg = batch_size / 2
                else:
                    self._index_in_epoch_neg = self._num_neg_examples
            end_neg = copy.deepcopy(self._index_in_epoch_neg)
            if self._index_in_epoch_pos > self._num_pos_examples:
                # Finished epoch
                self._epochs_completed += 1
                # Start next epoch
                start_pos = 0
                self._index_in_epoch_pos = batch_size / 2
                if (batch_size / 2) <= self._num_pos_examples:
                    self._index_in_epoch_pos = batch_size / 2
                else:
                    self._index_in_epoch_pos = self._num_pos_examples
            end_pos = copy.deepcopy(self._index_in_epoch_pos)
        
        start_pos = int(start_pos)
        end_pos = int(end_pos)
        start_neg = int(start_neg)
        end_neg = int(end_neg)
#         print ("start_pos: ", start_pos)
#         print ("end_pos: ", end_pos)
#         print ("start_neg: ", start_neg)
#         print ("end_neg: ", end_neg)
        
#         print("_____________________________")
#         print(self._data_pos)
#         print("_____________________________")
#         
#         for i in range(start_pos, end_pos):
#             print(self._data_pos[i])
#         print("_____________________________")
#             
#         print("self._data_pos[start_pos:end_pos, :]: ", self._data_pos[start_pos:end_pos, :])
#         print("self._data_neg[start_neg:end_neg, :]: ", self._data_neg[start_neg:end_neg, :])
#         print("np.ones((end_pos - start_pos, 1)), :]: ", np.ones((end_pos - start_pos, 1)))
#         print("np.ones((end_pos - start_pos, 1)), :]: ", np.zeros((end_neg - start_neg, 1)))

        return np.concatenate((self._data_pos[start_pos:end_pos, :], self._data_neg[start_neg:end_neg, :])), \
               np.concatenate((np.ones((end_pos - start_pos, 1)), np.zeros((end_neg - start_neg, 1))), axis=0)

    def save(self, filename):
        np.savez(filename, data=self._data, labels=self._labels, num_examples=self._num_examples,
                 index_in_epoch_neg=0, index_in_epoch_pos=0, epochs_completed=0)

    def load(self, filename):
        dataset = np.load(filename)
        self._data = dataset['data']
        self._labels = dataset['labels']
        self._num_examples = dataset['num_examples']
        self._index_in_epoch_neg = dataset['index_in_epoch_neg']
        self._index_in_epoch_neg = dataset['index_in_epoch_pos']
        self._epochs_completed = dataset['epochs_completed']
        self._data_pos = self._data[self._labels == 1, :]
        self._data_neg = self._data[self._labels == 0, :]


class MultiTaskDNN(object):
    def __init__(self, branch_term, category, input_width, shared_input_depth, spec_input_depth, dropout, mode,
                 shared_hidden_number, spec_hidden_number, num_task, output_number, nonlinearity_hidden, nonlinearity_output,
                 batch_size, share_num_epochs, spec_num_epochs, rng, lrate_shared, lrate_spec, momentum, optimizer,
                 model_file=None,
                 l1_lambda=0.0, l2_lambda=0.0):
        self.branch_term = branch_term
        self.category = category
        if shared_input_depth == len(shared_hidden_number):
            self.shared_input_depth = shared_input_depth
            self.shared_hidden_number = shared_hidden_number
        else:
            raise ValueError("shared layers: input_depth and hidden_number should match!")

        if spec_input_depth == len(spec_hidden_number):
            self.spec_input_depth = spec_input_depth
            self.spec_hidden_number = spec_hidden_number
        else:
            raise ValueError("specific layers: input_depth and hidden_number should match!")

        self.num_task = num_task
        self.input_width = input_width
        if batch_size != 0:
            self.batch_size = [batch_size] * num_task
            self.mini_batch = True
        else:
            self.batch_size = [0] * num_task
            self.mini_batch = False

        self.dropout = dropout
        self.mode = mode
        self.num_task = num_task
        self.output_number = output_number
        self.nonlinearity_hidden = nonlinearity_hidden
        self.nonlinearity_output = nonlinearity_output
        self.share_num_epochs = share_num_epochs
        self.spec_num_epochs = spec_num_epochs
        self.rng = rng
        self.lr_shared = theano.shared(np.array(lrate_shared, dtype=theano.config.floatX))
        self.lr_spec = theano.shared(np.array(lrate_spec, dtype=theano.config.floatX))
        self.lr_inc = np.array(1.2, dtype=theano.config.floatX)
        self.lr_dec = np.array(0.5, dtype=theano.config.floatX)
        self.momentum = momentum
        self.train_fn_array = []
        self.predict_fn_array = []
        self.valid_fn_array = []
        self.dnn_array = []
        self.train_spec_fn_array = []
        self.weights = []
        self.optimizer = optimizer

        self.sharedlayers = None
        self.spec_layers_array = []
        self.l1_lambda = l1_lambda
        self.l2_lambda = l2_lambda

        self.input_var = tensor.matrix('x')
        self.target_var = tensor.lvector('y')
        logging.info('[MTDNN][INIT] Initializing multi-task DNN:')
        for n in xrange(num_task):
            self.model_file = model_file

            logging.info('[MTDNN][INIT] Building the %d-th task...' % n)
            logging.info('[MTDNN][INIT]        batch_size is the %d...' % self.batch_size[n])

            if n == 0:
                stdnn = SingleTaskDNN(self.input_var, self.target_var, self.input_width, self.shared_input_depth,
                                      self.spec_input_depth, self.dropout,
                                      self.shared_hidden_number, self.spec_hidden_number, self.output_number,
                                      self.nonlinearity_hidden, self.nonlinearity_output, self.batch_size[n], self.rng,
                                      self.lr_shared, self.lr_spec, self.momentum, self.optimizer,
                                      None, None, self.l1_lambda, self.l2_lambda)
                self.sharedlayers = stdnn.get_shared_layers()
                self.dnn_array.append(stdnn.get_output_layer())
                self.spec_layers_array.append(stdnn.specific_layers)
                if self.mode == 'train':
                    train_fn, train_spec_fn, valid_fn = stdnn.build_functions_train()
                    self.train_fn_array.append(train_fn)
                    self.train_spec_fn_array.append(train_spec_fn)
                    self.valid_fn_array.append(valid_fn)
                    predict_fn = stdnn.build_functions_predict()
                    self.predict_fn_array.append(predict_fn)
                elif self.mode == 'predict':
                    predict_fn = stdnn.build_functions_predict()
                    self.predict_fn_array.append(predict_fn)
            else:
                stdnn = SingleTaskDNN(self.input_var, self.target_var, self.input_width, self.shared_input_depth,
                                      self.spec_input_depth, self.dropout,
                                      self.shared_hidden_number, self.spec_hidden_number, self.output_number,
                                      self.nonlinearity_hidden, self.nonlinearity_output, self.batch_size[n], self.rng,
                                      self.lr_shared, self.lr_spec, self.momentum, self.optimizer,
                                      self.sharedlayers, None,
                                      self.l1_lambda, self.l2_lambda)
                self.dnn_array.append(stdnn.get_output_layer())
                self.spec_layers_array.append(stdnn.specific_layers)
                if self.mode == 'train':
                    train_fn, train_spec_fn, valid_fn = stdnn.build_functions_train()
                    self.train_fn_array.append(train_fn)
                    self.train_spec_fn_array.append(train_spec_fn)
                    self.valid_fn_array.append(valid_fn)
                    predict_fn = stdnn.build_functions_predict()
                    self.predict_fn_array.append(predict_fn)
                elif self.mode == 'predict':
                    predict_fn = stdnn.build_functions_predict()
                    self.predict_fn_array.append(predict_fn)

        for n in xrange(self.num_task):
            if self.model_file is not None:
                model_fn = self.model_file + '.' + str(n)
                if exists(model_fn):
                    os.remove(model_fn)
                self._model2file_individual(n, model_fn)

    def _model2file(self, file_name='multi_task_dnn.model'):
        logging.info('[MTDNN][SAVE] Saving model:')
        nnet_dict = dict()
        nnet_dict['shared_hidden_number'] = self.shared_hidden_number
        nnet_dict['spec_hidden_number'] = self.spec_hidden_number
        nnet_dict['dropout'] = self.dropout
        nnet_dict['num_task'] = self.num_task
        nnet_dict['shared_layers'] = get_all_param_values(self.sharedlayers)
        # save all tasks' parameters
        for n in xrange(self.num_task):
            dict_n = 'all_params_values_' + str(n)
            nnet_dict[dict_n] = get_all_param_values(self.dnn_array[n])
            # save specific layers parameters
            for i in xrange(len(self.spec_layers_array[n])):
                dict_n_spec = 'specific_layer_' + str(i) + '_task_' + str(n)
                params = self.spec_layers_array[n][i].get_params()
                nnet_dict[dict_n_spec] = [p.get_value() for p in params]

        with utils.smart_open(file_name, 'wb') as fp:
            pickle.dump(nnet_dict, fp)
            fp.flush()
        logging.info('[MTDNN][SAVE] Model saved!')

    def _model2file_individual(self, n_task, file_name='multi_task_dnn.model'):
        if n_task >= self.num_task:
            raise ValueError("_model2file_individual: The input task number is greater than the total number of tasks!")
        logging.info('[MTDNN][SAVE_INDIVI] Saving model: %d' % n_task)
        nnet_dict = dict()
        nnet_dict['shared_hidden_number'] = self.shared_hidden_number
        nnet_dict['spec_hidden_number'] = self.spec_hidden_number
        nnet_dict['dropout'] = self.dropout
        nnet_dict['num_task'] = self.num_task
        nnet_dict['shared_layers'] = get_all_param_values(self.sharedlayers)

        dict_n = 'all_params_values_' + str(n_task)
        nnet_dict[dict_n] = get_all_param_values(self.dnn_array[n_task])
        # # save specific layers parameters
        # for i in xrange(len(self.spec_layers_array[n_task])):
        #     dict_n_spec = 'specific_layer_' + str(i) + '_task_' + str(n_task)
        #     params = self.spec_layers_array[n_task][i].get_params()
        #     nnet_dict[dict_n_spec] = [p.get_value() for p in params]

        with utils.smart_open(file_name, 'wb') as fp:
            pickle.dump(nnet_dict, fp)
            fp.flush()
        logging.info('[MTDNN][SAVE_INDIVI] Model saved!')

    def _file2model_individual(self, n_task, file_name='multi_task_dnn.model'):
        if n_task >= self.num_task:
            raise ValueError("_file2model_individual: The input task number is greater than the total number of tasks!")
        with utils.smart_open(file_name, 'rb') as fp:
            nnet_dict = pickle.load(fp)
        self.shared_hidden_number = nnet_dict['shared_hidden_number']
        self.spec_hidden_number = nnet_dict['spec_hidden_number']
        self.dropout = nnet_dict['dropout']
        self.num_task = nnet_dict['num_task']
        set_all_param_values(self.sharedlayers, nnet_dict['shared_layers'])

        dict_n = 'all_params_values_' + str(n_task)
        set_all_param_values(self.dnn_array[n_task], nnet_dict[dict_n])
        logging.info('> [MTDNN][LAOD_INDIVI] Model for task %d loaded!' % n_task)

    def _file2model(self, file_name='multi_task_dnn.model'):
        logging.info('[MTDNN][LOAD] Loading model:')
        with utils.smart_open(file_name, 'rb') as fp:
            nnet_dict = pickle.load(fp)
        self.shared_hidden_number = nnet_dict['shared_hidden_number']
        self.spec_hidden_number = nnet_dict['spec_hidden_number']
        self.dropout = nnet_dict['dropout']
        self.num_task = nnet_dict['num_task']
        set_all_param_values(self.sharedlayers, nnet_dict['shared_layers'])
        for n in xrange(self.num_task):
            dict_n = 'all_params_values_' + str(n)
            set_all_param_values(self.dnn_array[n], nnet_dict[dict_n])
            # load specific layers parameters
            for i in xrange(len(self.spec_layers_array[n])):
                dict_n_spec = 'specific_layer_' + str(i) + '_task_' + str(n)
                values = nnet_dict[dict_n_spec]
                params = self.spec_layers_array[n][i].get_params()
                for p, v in zip(params, values):
                    if p.get_value().shape != v.shape:
                        raise ValueError("mismatch: parameter has shape %r but value to "
                                         "set has shape %r" %
                                         (p.get_value().shape, v.shape))
                    else:
                        p.set_value(v)
        logging.info('[MTDNN][LOAD] Model loaded!')

    def fit(self):
        all_data_x_fn = 'data/all_data_X.csv'
        all_data_x = pd.read_csv(all_data_x_fn, sep='\t', header=0, index_col=0)
        all_proteins_train = [p.replace('"', '') for p in all_data_x.index]
        all_data_x.index = all_proteins_train
        train_fn = 'data/train_sets/' + self.branch_term[0:2] + self.branch_term[3:] + '_train.csv'
        print("train_fn", train_fn)
        train_y = pd.read_csv(train_fn, sep='\t', header=0, index_col=0)
        proteins_train = [p for p in train_y.index if p in all_data_x.index]
        train_x = all_data_x.loc[proteins_train, :]
        train_y = train_y.loc[proteins_train, :]
        validate_fn = 'data/train_sets/' + self.branch_term[0:2] + self.branch_term[3:] + '_valid.csv'
        validate_y = pd.read_csv(validate_fn, sep='\t', header=0, index_col=0)
        proteins_validate = [p for p in validate_y.index if p in all_data_x.index]
        validate_x = all_data_x.loc[proteins_validate, :]
        validate_y = validate_y.loc[proteins_validate, :]

        train_set = {}
        train_error = {}
        train_batch_error = {}
        for task in train_y.columns:
            proteins_in_task = train_y.index[train_y.loc[:, task] != inf]
            train_set[task] = DataSet(train_x.loc[proteins_in_task, :].values, train_y.loc[proteins_in_task, task].values)
            train_error[task] = []
            train_batch_error[task] = []

        active_tasks = [n for n in xrange(self.num_task)]
        goterms = validate_y.columns
        predict_error = []
        max_f1 = []
        max_mcc = []
        current_f1 = []
        current_mcc = []
        min_predict_error = []
        for n in active_tasks:
            proteins_in_task = validate_y.index[validate_y.loc[:, goterms[n]] != inf]
            # print "GO terms: " + goterms[n]
            # print "proteins_in_task: " + proteins_in_task
            p, e = self.valid_fn_array[n](validate_x.loc[proteins_in_task, :],
                                          validate_y.loc[proteins_in_task, goterms[n]])
            predict_error.append(np.mean(e))
            pred = p[:, 1]
            new_pred_g = [utils.binary(v, 0.5) for v in pred]
            max_f1.append(f1_score(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g))
            cm = confusion_matrix(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
            # print "valid label: "
            # print validate_y.loc[proteins_in_task, goterms[n]].values
            # print "predict label: "
            # print new_pred_g
            tp = cm[1][1]
            fp = cm[1][0]
            fn = cm[0][1]
            tn = cm[0][0]
            max_mcc.append(utils.mcc_calc(tp, tn, fp, fn))
            min_predict_error.append(predict_error)
            current_f1.append(0)
            current_mcc.append(0)

        logging.info('[MTDNN][FIT] starting fitting the multi-task DNN:')
        continue_training = True
        finished_tasks = []
        while continue_training:
            if len(finished_tasks) == self.num_task:
                break
            shuffled_index = utils.shuffle_list(xrange(self.num_task))
            shuffled_col = train_y.columns[shuffled_index]
            for (task, n) in zip(shuffled_col, shuffled_index):
                if train_set[task].epochs_completed() < self.share_num_epochs:
                    current_epoch = train_set[task].epochs_completed()
                    while current_epoch == train_set[task].epochs_completed():
                        batch_x, batch_y = train_set[task].next_batch(self.batch_size[n])
                        if train_set[task].epochs_completed() > current_epoch:
                            proteins_in_task = validate_y.index[validate_y.loc[:, goterms[n]] != inf]
                            train_set[task].shuffle()
                            train_error[task].append(np.mean(train_batch_error[task]))
                            train_batch_error[task] = []
                            p, e = self.valid_fn_array[n](validate_x.loc[proteins_in_task, :],
                                                          validate_y.loc[proteins_in_task, goterms[n]])
                            pred = p[:, 1]
                            new_pred_g = [utils.binary(v, 0.5) for v in pred]
                            current_f1[n] = f1_score(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            cm = confusion_matrix(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            tp = cm[1][1]
                            fp = cm[1][0]
                            fn = cm[0][1]
                            tn = cm[0][0]
                            current_mcc[n] = utils.mcc_calc(tp, tn, fp, fn)
                            logging.info('[MTDNN][FIT] task {0:d}, {1:s}, epoch {2: d}, '
                                         'mcc {3:.3f}, max mcc {4:.3f}'.format(n, goterms[n], current_epoch, current_mcc[n], max_mcc[n]))
                        # Debug
                        # logging.info('[DEBUG][MultiTaskDNN.fit] %d, %d, %d' % (batch_x.shape[0], batch_x.shape[1], batch_y.shape[0]))
                        e = self.train_fn_array[n](batch_x, batch_y.ravel())
                        train_batch_error[task].append(e)
                else:
                    finished_tasks.append(task)
            if np.mean(max_mcc) < np.mean(current_mcc):
                max_mcc = copy.deepcopy(current_mcc)
                for (task, n) in zip(train_y.columns, xrange(self.num_task)):
                    self._model2file_individual(n, self.model_file + '.' + str(n))
                    logging.info('[MTDNN][FIT] task {0:d}, {1:s}, epoch {2: d}, training error {3:.3f}, '
                                 'mcc {5:.3f}, average mcc score {5:.3f}'.format(n, goterms[n], current_epoch, train_error[task][-1],
                                                                                 current_mcc[n], np.mean(current_mcc)))
            else:
                for n in active_tasks:
                    self._file2model_individual(n, self.model_file + '.' + str(n))

        for n in active_tasks:
            self._file2model_individual(n, self.model_file + '.' + str(n))

        finished_tasks = []
        while continue_training:
            if len(finished_tasks) == self.num_task:
                break
            for (task, n) in zip(train_y.columns, xrange(self.num_task)):
                if train_set[task].epochs_completed() < (self.share_num_epochs + self.spec_num_epochs):
                    current_epoch = train_set[task].epochs_completed()
                    while current_epoch == train_set[task].epochs_completed():
                        batch_x, batch_y = train_set[task].next_batch(self.batch_size[n])

                        if train_set[task].epochs_completed() > current_epoch:
                            proteins_in_task = validate_y.index[validate_y.loc[:, goterms[n]] != inf]
                            train_set[task].shuffle()
                            train_error[task].append(np.mean(train_batch_error[task]))
                            train_batch_error[task] = []
                            p, e = self.valid_fn_array[n](validate_x.loc[proteins_in_task, :],
                                                          validate_y.loc[proteins_in_task, goterms[n]])
                            pred = p[:, 1]
                            new_pred_g = [utils.binary(v, 0.5) for v in pred]
                            current_f1[n] = f1_score(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            cm = confusion_matrix(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            tp = cm[1][1]
                            fp = cm[1][0]
                            fn = cm[0][1]
                            tn = cm[0][0]
                            current_mcc[n] = utils.mcc_calc(tp, tn, fp, fn)
                            logging.info('[MTDNN][FIT] task {0:d}, {1:s}, epoch {2: d}, '
                                         'mcc {3:.3f}, max mcc {4:.3f}'.format(n, goterms[n], current_epoch, current_mcc[n], max_mcc[n]))
                            if max_mcc[n] < current_mcc[n]:
                                max_mcc[n] = current_mcc[n]
                                self._model2file_individual(n, self.model_file + '.' + str(n))
                                logging.info('[MTDNN][FIT] task {0:d}, {1:s}, epoch {2: d}, training error {3:.3f}, '
                                             'mcc scores {5:.3f}, average mcc score {5:.3f}'.format(n, goterms[n], current_epoch,
                                                                                                    train_error[task][-1],
                                                                                                    current_mcc[n], np.mean(current_mcc)))
                            else:
                                self._file2model_individual(n, self.model_file + '.' + str(n))

                        e = self.train_spec_fn_array[n](batch_x, batch_y.ravel())
                        train_batch_error[task].append(e)
                else:
                    finished_tasks.append(task)

        return train_error

    def fit_final(self):
        all_data_x_fn = 'data/all_data_X.csv'
        all_data_x = pd.read_csv(all_data_x_fn, sep='\t', header=0, index_col=0)
        all_proteins_train = [p.replace('"', '') for p in all_data_x.index]
        all_data_x.index = all_proteins_train
        train_fn = 'data/train_sets/' + self.branch_term[0:2] + self.branch_term[3:] + '_train.csv'
        train_y = pd.read_csv(train_fn, sep='\t', header=0, index_col=0)
        proteins_train = [p for p in train_y.index if p in all_data_x.index]
        train_x = all_data_x.loc[proteins_train, :]
        train_y = train_y.loc[proteins_train, :]

        test_fn = 'data/train_sets/' + self.branch_term[0:2] + self.branch_term[3:] + '_test.csv'
        test_y = pd.read_csv(test_fn, sep='\t', header=0, index_col=0)
        proteins_test = [p for p in test_y.index if p in all_data_x.index]
        test_x = all_data_x.loc[proteins_test, :]
        test_y = test_y.loc[proteins_test, :]

        frames = [train_x, test_x]
        train_x = pd.concat(frames, axis=0)
        frames = [train_y, test_y]
        train_y = pd.concat(frames, axis=0)

        validate_fn = 'data/train_sets/' + self.branch_term[0:2] + self.branch_term[3:] + '_valid.csv'
        validate_y = pd.read_csv(validate_fn, sep='\t', header=0, index_col=0)
        proteins_validate = [p for p in validate_y.index if p in all_data_x.index]
        validate_x = all_data_x.loc[proteins_validate, :]
        validate_y = validate_y.loc[proteins_validate, :]

        train_set = {}
        train_error = {}
        train_batch_error = {}
        for task in train_y.columns:
            proteins_in_task = train_y.index[train_y.loc[:, task] != inf]
            train_set[task] = DataSet(train_x.loc[proteins_in_task, :].values, train_y.loc[proteins_in_task, task].values)
            train_error[task] = []
            train_batch_error[task] = []

        active_tasks = [n for n in xrange(self.num_task)]
        goterms = validate_y.columns
        predict_error = []
        max_f1 = []
        max_mcc = []
        current_f1 = []
        current_mcc = []
        min_predict_error = []
        for n in active_tasks:
            proteins_in_task = validate_y.index[validate_y.loc[:, goterms[n]] != inf]
            p, e = self.valid_fn_array[n](validate_x.loc[proteins_in_task, :],
                                          validate_y.loc[proteins_in_task, goterms[n]])
            predict_error.append(np.mean(e))
            pred = p[:, 1]
            new_pred_g = [utils.binary(v, 0.5) for v in pred]
            max_f1.append(f1_score(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g))
            cm = confusion_matrix(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
            tp = cm[1][1]
            fp = cm[1][0]
            fn = cm[0][1]
            tn = cm[0][0]
            max_mcc.append(utils.mcc_calc(tp, tn, fp, fn))
            min_predict_error.append(predict_error)
            current_f1.append(0)
            current_mcc.append(0)

        logging.info('[MTDNN][FIT_FINAL] starting fitting the multi-task DNN:')
        continue_training = True
        finished_tasks = []
        while continue_training:
            if len(finished_tasks) == self.num_task:
                break
            shuffled_index = utils.shuffle_list(xrange(self.num_task))
            shuffled_col = train_y.columns[shuffled_index]
            for (task, n) in zip(shuffled_col, shuffled_index):
                if train_set[task].epochs_completed() < self.share_num_epochs:
                    current_epoch = train_set[task].epochs_completed()
                    while current_epoch == train_set[task].epochs_completed():
                        batch_x, batch_y = train_set[task].next_batch(self.batch_size[n])
                        if train_set[task].epochs_completed() > current_epoch:
                            proteins_in_task = validate_y.index[validate_y.loc[:, goterms[n]] != inf]
                            train_set[task].shuffle()
                            train_error[task].append(np.mean(train_batch_error[task]))
                            train_batch_error[task] = []
                            p, e = self.valid_fn_array[n](validate_x.loc[proteins_in_task, :],
                                                          validate_y.loc[proteins_in_task, goterms[n]])
                            pred = p[:, 1]
                            new_pred_g = [utils.binary(v, 0.5) for v in pred]
                            current_f1[n] = f1_score(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            cm = confusion_matrix(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            tp = cm[1][1]
                            fp = cm[1][0]
                            fn = cm[0][1]
                            tn = cm[0][0]
                            current_mcc[n] = utils.mcc_calc(tp, tn, fp, fn)
                            logging.info('[MTDNN][FIT_FINAL] task {0:d}, {1:s}, epoch {2: d}, '
                                         'mcc {3:.3f}, max mcc {4:.3f}'.format(n, goterms[n], current_epoch,
                                                                               current_mcc[n], max_mcc[n]))
                        # Debug
                        # logging.info('[DEBUG][MTDNN][FIT_FINAL]%d, %d, %d' % (batch_x.shape[0], batch_x.shape[1], batch_y.shape[0]))
                        e = self.train_fn_array[n](batch_x, batch_y.ravel())
                        train_batch_error[task].append(e)
                else:
                    finished_tasks.append(task)
            if np.mean(max_mcc) < np.mean(current_mcc):
                max_mcc = copy.deepcopy(current_mcc)
                for (task, n) in zip(train_y.columns, xrange(self.num_task)):
                    self._model2file_individual(n, self.model_file + '.' + str(n))
                    logging.info('[MTDNN][FIT_FINAL] task {0:d}, {1:s}, epoch {2: d}, training error {3:.3f}, '
                                 'mcc {5:.3f}, average mcc score {5:.3f}'.format(n, goterms[n], current_epoch, train_error[task][-1],
                                                                                 current_mcc[n], np.mean(current_mcc)))
            else:
                for n in active_tasks:
                    self._file2model_individual(n, self.model_file + '.' + str(n))

        for n in active_tasks:
            self._file2model_individual(n, self.model_file + '.' + str(n))

        finished_tasks = []
        while continue_training:
            if len(finished_tasks) == self.num_task:
                break
            for (task, n) in zip(train_y.columns, xrange(self.num_task)):
                if train_set[task].epochs_completed() < (self.share_num_epochs + self.spec_num_epochs):
                    current_epoch = train_set[task].epochs_completed()
                    while current_epoch == train_set[task].epochs_completed():
                        batch_x, batch_y = train_set[task].next_batch(self.batch_size[n])

                        if train_set[task].epochs_completed() > current_epoch:
                            proteins_in_task = validate_y.index[validate_y.loc[:, goterms[n]] != inf]
                            train_set[task].shuffle()
                            train_error[task].append(np.mean(train_batch_error[task]))
                            train_batch_error[task] = []
                            p, e = self.valid_fn_array[n](validate_x.loc[proteins_in_task, :],
                                                          validate_y.loc[proteins_in_task, goterms[n]])
                            pred = p[:, 1]
                            new_pred_g = [utils.binary(v, 0.5) for v in pred]
                            current_f1[n] = f1_score(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            cm = confusion_matrix(validate_y.loc[proteins_in_task, goterms[n]].values, new_pred_g)
                            tp = cm[1][1]
                            fp = cm[1][0]
                            fn = cm[0][1]
                            tn = cm[0][0]
                            current_mcc[n] = utils.mcc_calc(tp, tn, fp, fn)
                            logging.info('[MTDNN][FIT_FINAL] task {0:d}, {1:s}, epoch {2: d}, '
                                         'mcc {3:.3f}, max mcc {4:.3f}'.format(n, goterms[n], current_epoch,
                                                                               current_mcc[n], max_mcc[n]))
                            if max_mcc[n] < current_mcc[n]:
                                max_mcc[n] = current_mcc[n]
                                self._model2file_individual(n, self.model_file + '.' + str(n))
                                logging.info('[MTDNN][FIT_FINAL] task {0:d}, {1:s}, epoch {2: d}, training error {3:.3f}, '
                                             'mcc scores {5:.3f}, average mcc score {5:.3f}'.format(n, goterms[n], current_epoch,
                                                                                                    train_error[task][-1],
                                                                                                    current_mcc[n],
                                                                                                    np.mean(current_mcc)))
                            else:
                                self._file2model_individual(n, self.model_file + '.' + str(n))

                        e = self.train_spec_fn_array[n](batch_x, batch_y.ravel())
                        train_batch_error[task].append(e)
                else:
                    finished_tasks.append(task)

        return train_error

    def _load_stop_tasks(self, stop_task_list, file_name='multi_task_dnn.model'):
        with utils.smart_open(file_name, 'rb') as fp:
            nnet_dict = pickle.load(fp)
        for n in stop_task_list:
            # load specific layers parameters
            for i in xrange(len(self.spec_layers_array[n])):
                dict_n_spec = 'specific_layer_' + str(i) + '_task_' + str(n)
                values = nnet_dict[dict_n_spec]
                params = self.spec_layers_array[n][i].get_params()
                for p, v in zip(params, values):
                    if p.get_value().shape != v.shape:
                        raise ValueError("mismatch: parameter has shape %r but value to "
                                         "set has shape %r" %
                                         (p.get_value().shape, v.shape))
                    else:
                        p.set_value(v)

    def _load_all_params(self, file_name='multi_task_dnn.model'):
        with utils.smart_open(file_name, 'rb') as fp:
            nnet_dict = pickle.load(fp)
        for n in xrange(self.num_task):
            dict_n = 'all_params_values_' + str(n)
            set_all_param_values(self.dnn_array[n], nnet_dict[dict_n])
            # load specific layers parameters
            for i in xrange(len(self.spec_layers_array[n])):
                dict_n_spec = 'specific_layer_' + str(i) + '_task_' + str(n)
                values = nnet_dict[dict_n_spec]
                params = self.spec_layers_array[n][i].get_params()
                for p, v in zip(params, values):
                    if p.get_value().shape != v.shape:
                        raise ValueError("mismatch: parameter has shape %r but value to "
                                         "set has shape %r" %
                                         (p.get_value().shape, v.shape))
                    else:
                        p.set_value(v)

    def set_weights(self):
        train_fn = 'data/train_sets/' + self.branch_term[0:2] + self.branch_term[3:] + '_train.csv'
        train_y = pd.read_csv(train_fn, sep='\t', header=0, index_col=0)
        for go in train_y.columns:
            num_pos = len(train_y.loc[train_y.loc[:, go] == 1, go])
            num_neg = len(train_y.loc[train_y.loc[:, go] == 0, go])
            self.weights.append((float(num_neg)/num_pos))

    def _softmax(self, a, b, w):
        # sb = np.divide(np.exp(b), np.exp(a * w) + np.exp(b))
        sb = np.divide(b, a * (2 ** w) + b)
        return sb

    def predict(self, test_x_array, model_file=None):
        active_tasks = [n for n in xrange(self.num_task)]
        pred_array = [[]] * self.num_task
        for n in active_tasks:
            self._file2model_individual(n, self.model_file + '.' + str(n))
            p = self.predict_fn_array[n](test_x_array)
            pred_array[n] = self._softmax(p[0][:, 0], p[0][:, 1], self.weights[n])
            logging.info('[MTDNN][PREDICT] task %d, prediction is done!' % n)
        return pred_array

