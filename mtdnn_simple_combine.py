#!/home/rui/anaconda2/bin/python

'''
This script produces three simple combination results from raw MTDNN prediction for the GO terms
appearing in two or more than two branches
1, using the maximum value
2, using the average value
3, using the minimun value
'''

from utils import *
import pandas as pd
import numpy as np
import sys

def xrange(x):
    return iter(range(x))

def combine_maximum(category='P'):
    all_pred = pd.read_csv('data/all_prediction_cafa3.csv', sep='\t', header=0, index_col=0)
    all_proteins = [p.replace('"', '') for p in all_pred.index]
    all_pred.index = all_proteins
    exp_goterms_label = all_pred.columns
    if category == 'P':
        fn_prefix = 'data/sub/bp_sub_'
        branches = read_txt_list('data/sub/bp_branches.txt')
    elif category == 'F':
        fn_prefix = 'data/sub/mf_sub_'
        branches = read_txt_list('data/sub/mf_branches.txt')
    elif category == 'C':
        fn_prefix = 'data/sub/cc_sub_'
        branches = read_txt_list('data/sub/cc_branches.txt')
    else:
        print "Error category!"
        sys.exit(-1)

    goterms = []
    for b in branches:
        fn = fn_prefix + b[0:2] + b[3:] + '.txt'
        goterms.extend(read_txt_list(fn))
    goterms = list(set(goterms))

    final_pred = pd.DataFrame(0, index=all_proteins, columns=goterms)
    for goterm in goterms:
        chosen_goterm = [g for g in exp_goterms_label if goterm in g]
        if len(chosen_goterm) == 1:
            final_pred.loc[:, goterm] = all_pred.loc[:, chosen_goterm].values
        else:
            final_pred.loc[:, goterm] = np.max(all_pred.loc[:, chosen_goterm], axis=1)
    output_path = 'outputs/final/'
    predict_fn = join(output_path, 'combined_maximum_final_' + category + '.predict')
    save_prediction(predict_fn, final_pred)
    return predict_fn


def combine_minimum(category='P'):
    all_pred = pd.read_csv('data/all_prediction_cafa3.csv', sep='\t', header=0, index_col=0)
    all_proteins = [p.replace('"', '') for p in all_pred.index]
    all_pred.index = all_proteins
    exp_goterms_label = all_pred.columns
    if category == 'P':
        fn_prefix = 'data/sub/bp_sub_'
        branches = read_txt_list('data/sub/bp_branches.txt')
    elif category == 'F':
        fn_prefix = 'data/sub/mf_sub_'
        branches = read_txt_list('data/sub/mf_branches.txt')
    elif category == 'C':
        fn_prefix = 'data/sub/cc_sub_'
        branches = read_txt_list('data/sub/cc_branches.txt')
    else:
        print "Error category!"
        sys.exit(-1)

    goterms = []
    for b in branches:
        fn = fn_prefix + b[0:2] + b[3:] + '.txt'
        goterms.extend(read_txt_list(fn))
    goterms = list(set(goterms))

    final_pred = pd.DataFrame(0, index=all_proteins, columns=goterms)
    for goterm in goterms:
        chosen_goterm = [g for g in exp_goterms_label if goterm in g]
        if len(chosen_goterm) == 1:
            final_pred.loc[:, goterm] = all_pred.loc[:, chosen_goterm].values
        else:
            final_pred.loc[:, goterm] = np.min(all_pred.loc[:, chosen_goterm], axis=1)
    output_path = 'outputs/final/'
    predict_fn = join(output_path, 'combined_minimum_final_' + category + '.predict')
    save_prediction(predict_fn, final_pred)
    return predict_fn


def combine_average(category='P'):
    all_pred = pd.read_csv('data/all_prediction_cafa3.csv', sep='\t', header=0, index_col=0)
    all_proteins = [p.replace('"', '') for p in all_pred.index]
    all_pred.index = all_proteins
    exp_goterms_label = all_pred.columns
    if category == 'P':
        fn_prefix = 'data/sub/bp_sub_'
        branches = read_txt_list('data/sub/bp_branches.txt')
    elif category == 'F':
        fn_prefix = 'data/sub/mf_sub_'
        branches = read_txt_list('data/sub/mf_branches.txt')
    elif category == 'C':
        fn_prefix = 'data/sub/cc_sub_'
        branches = read_txt_list('data/sub/cc_branches.txt')
    else:
        print "Error category!"
        sys.exit(-1)

    goterms = []
    for b in branches:
        fn = fn_prefix + b[0:2] + b[3:] + '.txt'
        goterms.extend(read_txt_list(fn))
    goterms = list(set(goterms))

    final_pred = pd.DataFrame(0, index=all_proteins, columns=goterms)
    for goterm in goterms:
        chosen_goterm = [g for g in exp_goterms_label if goterm in g]
        if len(chosen_goterm) == 1:
            final_pred.loc[:, goterm] = all_pred.loc[:, chosen_goterm].values
        else:
            final_pred.loc[:, goterm] = np.mean(all_pred.loc[:, chosen_goterm], axis=1)
    output_path = 'outputs/final/'
    predict_fn = join(output_path, 'combined_average_final_' + category + '.predict')
    save_prediction(predict_fn, final_pred)
    return predict_fn


# def combine_bayesian():

if __name__ == "__main__":
    predict_fn_p = combine_maximum('P')
    predict_fn_f = combine_maximum('F')
    predict_fn_c = combine_maximum('C')
    # reformat(predict_fn_p)
    # reformat(predict_fn_f)
    # reformat(predict_fn_c)

    predict_fn_p = combine_minimum('P')
    predict_fn_f = combine_minimum('F')
    predict_fn_c = combine_minimum('C')
    # reformat(predict_fn_p)
    # reformat(predict_fn_f)
    # reformat(predict_fn_c)

    predict_fn_p = combine_average('P')
    predict_fn_f = combine_average('F')
    predict_fn_c = combine_average('C')
    # reformat(predict_fn_p)
    # reformat(predict_fn_f)
    # reformat(predict_fn_c)


