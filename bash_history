virtualenv mtdnn/
cd mtdnn
source bin/activate
pip install theano
pip install numpy
pip install lasagne
pip install --upgrade https://github.com/Theano/Theano/archive/master.zip
pip install --upgrade https://github.com/Lasagne/Lasagne/archive/master.zip
pip install biopython
pip install pandas
pip install pickle
pip install hyperopt
pip install getopt
pip install logging
pip install sklearn

pip install kin
pip install go_tree
pip install obonet
conda install mkl-service
python mtdnn_hyperopt.py -b GO00000003 -c bp
sudo vi /etc/ld.so.conf
sudo  /sbin/ldconfig -v
python mtdnn_hyperopt.py -b GO00000003 -c bp

pip install tables
pip install keras
pip install tensorflow

python mtdnn_train.py -b GO00000003 -s 10 -l logfile_mtdnn_train
python mtdnn_predict.py -b GO00000003 -m 'train' -i data/all_data_X.csv -l logfile_mtdnn_predict
