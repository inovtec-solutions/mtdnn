#!/usr/bin/python

import theano
# import theano.tensor as T
import lasagne
from lasagne.layers import get_output, DropoutLayer, InputLayer, DenseLayer, get_all_params, batch_norm
from lasagne.init import GlorotUniform
from lasagne.objectives import categorical_crossentropy
from lasagne.updates import nesterov_momentum, adagrad, rmsprop, adam, adadelta
from lasagne.regularization import regularize_network_params, l1, l2, regularize_layer_params
import numpy as np
import sys

def xrange(x):
    return iter(range(x))

class SingleTaskDNN(object):
    def __init__(self, input_var, target_var, input_width, shared_input_depth, spec_input_depth,
                 dropout, shared_hidden_number, spec_hidden_number, output_number,
                 nonlinearity_hidden, nonlinearity_output, batch_size, rng, lrate_shared, lrate_spec, momentum, optimizer,
                 sharedlayers=None, spec_layers=None, l1_lambda=0.0, l2_lambda=0.0):
        self.input_width = input_width
        self.batch_size = batch_size
        self.shared_input_depth = shared_input_depth
        self.spec_input_depth = spec_input_depth
        self.shared_hidden_number = shared_hidden_number
        self.spec_hidden_number = spec_hidden_number
        self.dropout = dropout
        self.output_number = output_number
        self.nonlinearity_hidden = nonlinearity_hidden
        self.nonlinearity_output = nonlinearity_output
        self.rng = rng
        self.current_lr_shared = lrate_shared
        self.current_lr_spec = lrate_spec
        self.optimizer = optimizer
        self.momentum = momentum
        self.sharedlayers = sharedlayers
        self.specific_layers = spec_layers
        self.L1_lambda = l1_lambda
        self.L2_lambda = l2_lambda

        lasagne.random.set_rng(self.rng)
        self.input_var = input_var
        self.target_var = target_var

        self.l_out = self.build_network()

    def build_network(self):

        if self.sharedlayers is None:
            l_in = InputLayer(shape=(None, self.input_width), input_var=self.input_var)
            l_in_drop = DropoutLayer(l_in, p=self.dropout[0])
            shared_layers = [l_in_drop]
            for i in xrange(self.shared_input_depth):
                l_hid = batch_norm(DenseLayer(shared_layers[i], num_units=self.shared_hidden_number[i],
                                              nonlinearity=self.nonlinearity_hidden, W=GlorotUniform()))
                l_hid_drop = DropoutLayer(l_hid, p=self.dropout[1])
                shared_layers.append(l_hid_drop)
            self.sharedlayers = shared_layers
            specific_layers = []
            l_hid_drop = shared_layers[-1]
            for i in xrange(self.spec_input_depth):
                l_hid = batch_norm(DenseLayer(l_hid_drop, num_units=self.spec_hidden_number[i],
                                              nonlinearity=self.nonlinearity_hidden, W=GlorotUniform()))
                l_hid_drop = DropoutLayer(l_hid, p=self.dropout[1])
                specific_layers.append(l_hid_drop)
            self.specific_layers = specific_layers
            l_out = batch_norm(DenseLayer(self.specific_layers[-1], num_units=self.output_number,
                                          nonlinearity=self.nonlinearity_output))

        else:
            self.specific_layers = []
            l_hid_drop = self.sharedlayers[-1]
            for i in xrange(self.spec_input_depth):
                l_hid = batch_norm(DenseLayer(l_hid_drop, num_units=self.spec_hidden_number[i],
                                              nonlinearity=self.nonlinearity_hidden, W=GlorotUniform()))
                l_hid_drop = DropoutLayer(l_hid, p=self.dropout[1])
                self.specific_layers.append(l_hid_drop)
            self.specific_layers = self.specific_layers
            l_out = batch_norm(DenseLayer(self.specific_layers[-1], num_units=self.output_number,
                                          nonlinearity=self.nonlinearity_output))
        return l_out

    def build_functions_train(self):
        # index = T.lscalar('index')
        prediction = get_output(self.l_out)
        loss_t = categorical_crossentropy(prediction, self.target_var)
        loss = loss_t.mean()
        params = get_all_params(self.l_out, trainable=True)
        if self.L1_lambda != 0.0:
            l1_loss = 0
            for layer in self.specific_layers:
                l1_loss += regularize_layer_params(layer, l1)
            l1_loss += regularize_layer_params(self.l_out, l1)
            loss += self.L1_lambda * l1_loss
        if self.L2_lambda != 0.0:
            l2_loss = regularize_network_params(self.l_out, l2)
            loss += self.L2_lambda * l2_loss

        loss_spec = loss_t.mean()
        if self.L1_lambda != 0.0:
            l1_loss = 0
            for layer in self.specific_layers:
                l1_loss += regularize_layer_params(layer, l1)
            l1_loss += regularize_layer_params(self.l_out, l1)
            loss_spec += self.L1_lambda * l1_loss

        params_specific = sum([l.get_params(trainable=True) for l in self.specific_layers], [])
        if self.optimizer == 'nesterov_momentum':
            updates = nesterov_momentum(loss, params, learning_rate=self.current_lr_shared, momentum=self.momentum)
            updates_specific = nesterov_momentum(loss_spec, params_specific, learning_rate=self.current_lr_spec,
                                                 momentum=self.momentum)
        elif self.optimizer == 'adagrad':
            updates = adagrad(loss, params, learning_rate=self.current_lr_shared)
            updates_specific = adagrad(loss, params, learning_rate=self.current_lr_spec)
        elif self.optimizer == 'adadelta':
            updates = adadelta(loss, params, learning_rate=self.current_lr_shared)
            updates_specific = adadelta(loss, params, learning_rate=self.current_lr_spec)
        elif self.optimizer == 'adam':
            updates = adam(loss, params, learning_rate=self.current_lr_shared)
            updates_specific = adam(loss, params, learning_rate=self.current_lr_spec)
        elif self.optimizer == 'rmsprop':
            updates = rmsprop(loss, params, learning_rate=self.current_lr_shared)
            updates_specific = rmsprop(loss, params, learning_rate=self.current_lr_spec)
        else:
            print ("[SingleTask]{ERROR] optimizer")
            sys.exit(-1)

        test_prediction = get_output(self.l_out, deterministic=True)
        test_loss = categorical_crossentropy(test_prediction, self.target_var)

        _train_fn = theano.function(inputs=[self.input_var, self.target_var],
                                    outputs=loss,
                                    updates=updates,
                                    allow_input_downcast=True)
        _train_spec_fn = theano.function(inputs=[self.input_var, self.target_var],
                                         outputs=loss_spec,
                                         updates=updates_specific,
                                         allow_input_downcast=True)
        _valid_fn = theano.function(inputs=[self.input_var, self.target_var],
                                    outputs=[test_prediction, test_loss],
                                    allow_input_downcast=True)

        return _train_fn, _train_spec_fn, _valid_fn

    def build_functions_predict(self):
        test_prediction = get_output(self.l_out, deterministic=True)
        _predic_fn = theano.function(inputs=[self.input_var],
                                     outputs=[test_prediction],
                                     allow_input_downcast=True)
        return _predic_fn

    def get_output_layer(self):
        return self.l_out

    def get_shared_layers(self):
        return self.sharedlayers
