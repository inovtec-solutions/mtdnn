from multitask import *
import sys
from utils import *
from lasagne.nonlinearities import rectify, softmax
from os.path import exists
from os import makedirs
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
import logging
import getopt

INPUT_WIDTH = 258

def xrange(x):
    return iter(range(x))

space = {'shared_input_depth': hp.choice('shared_input_depth', [1, 2, 3]),
         'spec_input_depth': hp.choice('spec_input_depth', [1, 2, 3]),
         'dropout_i': hp.uniform('dropout_i', .05, .95),
         'dropout_o': hp.uniform('dropout_o', .05, .95),
         'shared_hidden_number_1': hp.choice('shared_hidden_number_1', [256, 512, 768, 1024]),
         'shared_hidden_number_2': hp.choice('shared_hidden_number_2', [256, 512, 768, 1024]),
         'shared_hidden_number_3': hp.choice('shared_hidden_number_3', [256, 512, 768, 1024]),
         'spec_hidden_number_1': hp.choice('spec_hidden_number_1', [64, 128, 256, 512]),
         'spec_hidden_number_2': hp.choice('spec_hidden_number_2', [64, 128, 256, 512]),
         'spec_hidden_number_3': hp.choice('spec_hidden_number_3', [64, 128, 256, 512]),
         'batch_size': hp.choice('batch_size', [32, 64, 128, 256]),
         'shared_epochs': 50,
         'spec_epochs': 50,
         'shared_lrate': hp.uniform('shared_lrate', 1e-3, 0.9),
         'spec_lrate': hp.uniform('spec_lrate', 1e-3, 0.9),
         'momentum': hp.uniform('momentum', 0.01, 1.0),
         'optimizer': hp.choice('optimizer', ['nesterov_momentum', 'adagrad', 'adadelta', 'adam', 'rmsprop']),
         'hid_activation': rectify,
         'out_activation': softmax,
         'l1_lambda': hp.choice('l1_lambda', [0, 0.00025, 0.0005, 0.00075, 0.0015, 0.005]),
         'l2_lambda': hp.choice('l2_lambda', [0, 0.00025, 0.0005, 0.00075, 0.0015, 0.005])
         }


def f_nn(params):
    logging.info('[MTDNN][PARAMS] Params testing: ')
    for k in params.keys():
        logging.info('\t\t %s\t%s' % (k, params[k]))
    category = params['category']
    branch = params['branch']
    shared_input_depth = params['shared_input_depth']
    if shared_input_depth == 1:
        shared_hidden_units = [params['shared_hidden_number_1']]
    elif shared_input_depth == 2:
        shared_hidden_units = [params['shared_hidden_number_1'], params['shared_hidden_number_2']]
    elif shared_input_depth == 3:
        shared_hidden_units = [params['shared_hidden_number_1'], params['shared_hidden_number_2'],
                               params['shared_hidden_number_3']]
    else:
        print ("Error: shared_input_depth")
        sys.exit(-1)

    spec_input_depth = params['spec_input_depth']
    if spec_input_depth == 1:
        spec_hidden_units = [params['spec_hidden_number_1']]
    elif spec_input_depth == 2:
        spec_hidden_units = [params['spec_hidden_number_1'], params['spec_hidden_number_2']]
    elif spec_input_depth == 3:
        spec_hidden_units = [params['spec_hidden_number_1'], params['spec_hidden_number_2'],
                             params['spec_hidden_number_3']]
    else:
        print ("Error: spec_input_depth")
        sys.exit(-1)

    optimizer = params['optimizer']
    shared_lrate = params['shared_lrate']
    spec_lrate = params['spec_lrate']
    dropout = [params['dropout_i'], params['dropout_o']]
    l1_lambda = params['l1_lambda']
    l2_lambda = params['l2_lambda']
    shared_epochs = params['shared_epochs']
    spec_epochs = params['spec_epochs']
    batch_size = params['batch_size']
    nonlinearity_hidden = params['hid_activation']
    nonlinearity_output = params['out_activation']
    rng = np.random.RandomState(0)
    mode = 'train'
    momentum = params['momentum']

    branch_fn = 'data/sub/' + category + '_sub_' + branch[0:2] + branch[3:] + '.txt'
    print("branch_fn: ",branch_fn)
    if not exists(branch_fn):
        print ("Error: can't load branch " + branch)
        sys.exit(-1)
    with open(branch_fn, 'r') as f:
        lines = f.readlines()
        num_task = len(lines)

    model_file = 'models/mtdnn_' + branch[0:2] + branch[3:] + 'hyperopt.model'

    mtdnn = MultiTaskDNN(branch, category, input_width=INPUT_WIDTH, shared_input_depth=shared_input_depth,
                         spec_input_depth=spec_input_depth, dropout=dropout, mode=mode, shared_hidden_number=shared_hidden_units,
                         spec_hidden_number=spec_hidden_units, num_task=num_task, output_number=2,
                         nonlinearity_hidden=nonlinearity_hidden, nonlinearity_output=nonlinearity_output,
                         batch_size=batch_size, share_num_epochs=shared_epochs, spec_num_epochs=spec_epochs,
                         rng=rng, lrate_shared=shared_lrate, lrate_spec=spec_lrate, momentum=momentum, optimizer=optimizer,
                         model_file=model_file, l1_lambda=l1_lambda, l2_lambda=l2_lambda)
    mtdnn.fit()
    test_data, test_label = load_test_data(branch)
    mtdnn.set_weights()
    pred_y = mtdnn.predict(test_data)
    f1 = f1_score_array(test_label.values, np.array(pred_y))
    logging.info('[MMLDNN][F1] %.3f' % f1)
    return {'loss': -f1, 'status': STATUS_OK}


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hb:c:", ["branch_term=", "category="])
    except getopt.GetoptError:
        print ('mtdnn_hyperopt.py -b <branch_term> -c <category>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print ('mtdnn_hyperopt.py -b <branch_term>')
            sys.exit()
        elif opt in ("-b", "--branch_term"):
            branch_term = arg
        elif opt in ("-c", "--category"):
            category = arg
        else:
            print ('mtdnn_hyperopt.py -b <branch_term>')
            sys.exit()

    if not exists('log'):
        makedirs('log')
    log_gfile = 'log/mtdnn_' + category + '_' + branch_term[0:2] + branch_term[3:] + '_hyperopt.log'
    logging.basicConfig(filename=log_gfile, format='%(asctime)s %(message)s', level=logging.INFO)
    trials = Trials()
    space['category'] = category
    space['branch'] = branch_term
    best = fmin(f_nn, space, algo=tpe.suggest, max_evals=100, trials=trials)
    logging.info('[MTDNN][BEST] Best model:')
    for k in best.keys():
        logging.info('\t\t %s\t%s' % (k, best[k]))
    save_dict(best, 'outputs/best_hyper_params.dict')


if __name__ == "__main__":
    main(sys.argv[1:])