#!/home/rui/anaconda2/bin/python

from multitask import MultiTaskDNN
from utils import *
from lasagne.nonlinearities import rectify, softmax
import sys
import getopt
from os.path import exists, dirname, join
from os import makedirs
from io import StringIO
import numpy as np
import pandas as pd
from numpy import inf
import json
import logging
import pickle
from utils import multi_task_performance_measure
import tables
from keras.optimizers import SGD, RMSprop

def xrange(x):
    return iter(range(x))

def main(argv):
    # logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    try:
        opts, args = getopt.getopt(argv, "hb:s:l:", ["branch_term=", "setting=", "logfile="])
    except getopt.GetoptError:
        print ( 'mtdnn_train.py -b <branch_term> -s <setting> -l <logfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print ( 'mtdnn_train.py -b <branch_term> -s <setting> -l <logfile>')
            sys.exit()
        elif opt in ("-b", "--branch_term"):
            branch_term = arg
        elif opt in ("-s", "--setting"):
            setting = int(arg)
        elif opt in ("-l", "--logfile"):
            log_gfile = arg
    logging.basicConfig(filename=log_gfile, format='%(asctime)s %(message)s', level=logging.INFO)
    #configs_fn = 'configs/' + branch_term[0:2] + branch_term[3:] + '_mtdnn_setting_' + str(setting - 1) + '.dict'
    configs_fn = 'configs/' + branch_term[0:2] + branch_term[3:] + '_mtdnn_final' + '.dict'
    
    print("configs_fn: ", configs_fn)
    
    "configs/GO0000003_mtdnn_setting_9"
    with smart_open(configs_fn, 'rb') as fp:
        configs = pickle.load(fp)
    input_width = configs['input_width']
    shared_input_depth = configs['shared_input_depth']
    spec_input_depth = configs['spec_input_depth']
    exp_type = configs['exp_type']
    dropout = configs['dropout']
    shared_hidden_number = configs['shared_hidden_number']
    spec_hidden_number = configs['spec_hidden_number']
    output_number = configs['output_number']
    nonlinearity_hidden = rectify
    nonlinearity_output = softmax
    batch_size = configs['batch_size']
    num_task = configs['num_task']
    share_num_epochs = configs['share_num_epochs']
    spec_num_epochs = configs['spec_num_epochs']
    lrate = configs['lrate']
    momentum = configs['momentum']
    l1_lambda = configs['l1_lambda']
    l2_lambda = configs['l2_lambda']
    category = configs['category']
    if 'output_path' in configs.keys():
        d = dirname(configs['output_path'])
        output_path = configs['output_path']
        if not exists(d):
            makedirs(d)
    else:
        logging.info('[ERROR] ''output_path'' has to be specified!')
        sys.exit(2)
    predict_fn = join(output_path, branch_term[0:2] + branch_term[3:] + '_' + category + '.predict')
    metrics_fn = join(output_path, branch_term[0:2] + branch_term[3:] + '_' + category + '.metrics')
    if 'model_path' in configs.keys():
        d = dirname(configs['model_path'])
        model_path = configs['model_path']
        if not exists(d):
            makedirs(d)
    else:
        logging.info('[ERROR] ''model_path'' has to be specified!')
        sys.exit(2)

    if exp_type == 'train':
        logging.info('[MTDNN][INIT] Initializing ...')
        random_state = np.random.RandomState(0)
        print('l1_lambda: ', l1_lambda)
        print('l2_lambda: ', l2_lambda)
        print('lrate: ', lrate)
       
        sgd=SGD(lr=lrate)
        dfp = MultiTaskDNN(branch_term, category, input_width=input_width, shared_input_depth=shared_input_depth,
                           spec_input_depth=spec_input_depth, dropout=dropout, mode=exp_type, shared_hidden_number=shared_hidden_number,
                           spec_hidden_number=spec_hidden_number, num_task=num_task, output_number=output_number,
                           nonlinearity_hidden=nonlinearity_hidden, nonlinearity_output=nonlinearity_output,
                           batch_size=batch_size, share_num_epochs=share_num_epochs, spec_num_epochs=spec_num_epochs,
                           rng=random_state, lrate_shared=lrate, lrate_spec=lrate, momentum=momentum,
                           optimizer='adam',
                           model_file=join(model_path + 'mtdnn.model'), l1_lambda=l1_lambda, l2_lambda=l2_lambda)
        
        
        logging.info('[MTDNN][FIT] fitting ...')
        dfp.fit()
        logging.info('[MTDNN][PREDICT] predicting ...')
        del dfp
        exp_type = 'predict'
        random_state = np.random.RandomState(0)
        dfp_predict = MultiTaskDNN(branch_term, category, input_width=input_width, shared_input_depth=shared_input_depth,
                                   spec_input_depth=spec_input_depth, dropout=dropout, mode=exp_type,
                                   shared_hidden_number=shared_hidden_number, spec_hidden_number=spec_hidden_number,
                                   num_task=num_task, output_number=output_number, nonlinearity_hidden=nonlinearity_hidden,
                                   nonlinearity_output=nonlinearity_output, batch_size=batch_size,
                                   share_num_epochs=share_num_epochs, spec_num_epochs=spec_num_epochs, rng=random_state,
                                   lrate_shared=lrate, lrate_spec=lrate, momentum=momentum, 
                                   optimizer='adam',
                                   model_file=join(model_path + 'mtdnn.model'),
                                   l1_lambda=l1_lambda, l2_lambda=l2_lambda)

        all_data_x_fn = 'data/all_data_X.csv'
        all_data_x = pd.read_csv(all_data_x_fn, sep='\t', header=0, index_col=0)
        all_proteins_train = [p.replace('"', '') for p in all_data_x.index]
        all_data_x.index = all_proteins_train
        test_fn = 'data/train_sets/' + branch_term[0:2] + branch_term[3:] + '_test.csv'
        test_y = pd.read_csv(test_fn, sep='\t', header=0, index_col=0)
        proteins_test = [p for p in test_y.index if p in all_data_x.index]
        test_x = all_data_x.loc[proteins_test, :]
        test_y = test_y.loc[proteins_test, :]
        dfp_predict.set_weights()
        pred_y = dfp_predict.predict(test_x)
        pred_y = np.array(pred_y).transpose()
        metrics = multi_task_performance_measure(test_y, pred_y)
        
        print("predict_fn: ", predict_fn)
        print("metrics_fn: ", metrics_fn)

        with tables.open_file(predict_fn, 'w') as h5_file:
            h5_file.create_array('/', 'pred_y', pred_y)
            h5_file.create_array('/', 'test_y', test_y.values)
        with smart_open(metrics_fn, 'wb') as f:
            pickle.dump(metrics, f)
            f.flush()
        logging.info('[MTDNN][PREDICT] predicting finished!')

    elif exp_type == 'predict':
        logging.info('[MTDNN][PREDICT] Starting training')
        random_state = np.random.RandomState(0)

        dfp = MultiTaskDNN(branch_term, category, input_width=input_width, shared_input_depth=shared_input_depth,
                           spec_input_depth=spec_input_depth, dropout=dropout, mode=exp_type, shared_hidden_number=shared_hidden_number,
                           spec_hidden_number=spec_hidden_number, num_task=num_task, output_number=output_number,
                           nonlinearity_hidden=nonlinearity_hidden, nonlinearity_output=nonlinearity_output,
                           batch_size=batch_size, share_num_epochs=share_num_epochs, spec_num_epochs=spec_num_epochs,
                           rng=random_state, lrate_shared=lrate, lrate_spec=lrate, momentum=momentum,
                           optimizer='adam',
                           model_file=join(model_path + 'mtdnn.model'), l1_lambda=l1_lambda, l2_lambda=l2_lambda)
        all_data_x_fn = 'data/all_data_X.csv'
        all_data_x = pd.read_csv(all_data_x_fn, sep='\t', header=0, index_col=0)
        test_fn = 'data/train_sets/' + branch_term[0:2] + branch_term[3:] + '_test.csv'
        test_y = pd.read_csv(test_fn, sep='\t', header=0, index_col=0)
        test_x = all_data_x.loc[test_y.index, :]
        dfp.set_weights()
        pred_y = dfp.predict(test_x)
        pred_y = np.array(pred_y).transpose()
        metrics = multi_task_performance_measure(test_y, pred_y)
        with tables.open_file(predict_fn, 'w') as h5_file:
            h5_file.create_array('/', 'pred_y', pred_y)
            h5_file.create_array('/', 'test_y', test_y.values)
        with smart_open(metrics_fn, 'w') as f:
            pickle.dump(metrics, f)
            f.flush()
        logging.info('[MTDNN][PREDICT] predicting finished!')

if __name__ == "__main__":
    main(sys.argv[1:])
