

#from go_tree import *
#from obo_parser import GODag
import copy
import pickle
from numpy import inf
import time
import string
import numpy as np
import pandas as pd
from sklearn.metrics import roc_curve, auc, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.metrics import matthews_corrcoef
from scipy import interp
from os import makedirs, system
from os.path import join, exists, splitext
from Bio import SwissProt as sp
import logging

def xrange(x):
    return iter(range(x))

readers = {}
try:
    import gzip
    readers['.gz'] = gzip.GzipFile
except ImportError:
    pass
try:
    import bz2
    readers['.bz2'] = bz2.BZ2File
except ImportError:
    pass


def import_data(filename, sep='\t', verbose=False):
    start_time = time.time()
    matrix = []
    row_header = []
    column_header = []
    first_row = True

    try:
        for line in open(filename, 'rU').xreadlines():
            t = string.split(line[:-1], sep=sep)  # remove end-of-line character - file is tab-delimited
            if first_row:
                column_header = t[1:]
                first_row = False
            else:
                if ' ' not in t and '' not in t:  # Occurs for rows with missing data
                    s = map(float, t[1:])
                    if (abs(max(s)-min(s))) > 0:
                        matrix.append(s)
                        row_header.append(t[0])
        if verbose:
            time_diff = str(round(time.time()-start_time, 1))
            print ('\n%d rows and %d columns imported for %s in %s seconds...' % (len(matrix), len(column_header),
                                                                                 filename, time_diff))

    except IOError:
        print ('No data in input file.')
    return np.array(matrix), column_header, row_header


##########################################################################################################
# smart_open
########################################################################################################
def smart_open(filename, mode='rb', *args, **kwargs):
    # Opens a file "smartly":
    #  * If the filename has a ".gz" or ".bz2" extension, compression is handled
    #    automatically;
    #  * If the file is to be read and does not exist, corresponding files with
    #    a ".gz" or ".bz2" extension will be attempted.
    # (The Python packages "gzip" and "bz2" must be installed to deal with the
    #    corresponding extensions)

    if 'r' in mode and not exists(filename):
        for ext in readers:
            if exists(filename + ext):
                filename += ext
                break
    extension = splitext(filename)[1]
    return readers.get(extension, open)(filename, mode, *args, **kwargs)


def read_txt_list(fn):
    res = []
    with open(fn, 'r') as f:
        for l in f:
            res.append(l[:-1])
    return res


def write_txt_list(fn, lines):
    with open(fn, 'w') as f:
        for l in lines:
            f.write("%s\n" % str(l))


def save_dict(dict, filename):
    with open(filename, 'w') as fp:
        pickle.dump(dict, fp)
        fp.flush()


def load_dict(filename):
    with open(filename, 'r') as fp:
        dict_mapp = pickle.load(fp)
    return dict_mapp


###############################################################################################################
# various measures
###############################################################################################################
def calc_precision_recall(test, pred, threshold, godag):
    bin_pred = [True if p >= threshold else False for p in pred]
    # Reference GO terms set
    ref_go_terms = test.index[test == 1]
    if type(ref_go_terms) == type('a'):
        ref_go_terms = [ref_go_terms]
    ref_go_terms_propogated = prop_go_terms_ancestors(ref_go_terms, godag)
    pred_go_terms = pred.index[bin_pred]
    if type(pred_go_terms) == type('a'):
        pred_go_terms = [pred_go_terms]
    pred_go_terms_propogated = prop_go_terms_ancestors(pred_go_terms, godag)

    if len(ref_go_terms) != 0:
        prec, reca = precision_recall_score(ref_go_terms_propogated, pred_go_terms_propogated)
    else:
        prec = 0.0
        reca = 0.0
    return prec, reca


def prop_go_terms_ancestors(go_terms, godag):
    go_terms_propogated = []
    for got in go_terms:
        got = got[0:2] + ':' + got[2:]
        rec = godag.query_term(got, verbose=False)
        a = rec.get_all_parent_edges()
        b = list(a)
        tmp = [g for i in range(len(b)) for g in b[i][:]]
        go_terms_propogated.extend(list(set(tmp)))
    go_terms_propogated = list(set(go_terms_propogated))
    if 'GO:0005575' in go_terms_propogated:
        go_terms_propogated.remove('GO:0005575')
    elif 'GO:0003674' in go_terms_propogated:
        go_terms_propogated.remove('GO:0003674')
    elif 'GO:0008150' in go_terms_propogated:
        go_terms_propogated.remove('GO:0008150')

    return go_terms_propogated


def prop_go_terms_offsprings(go_terms, godag):
    go_terms_propogated = []
    for got in go_terms:
        rec = godag.query_term(got, verbose=False)
        a = rec.get_all_child_edges()
        b = list(a)
        tmp = [g for i in range(len(b)) for g in b[i][:]]
        go_terms_propogated.extend(list(set(tmp)))
    go_terms_propogated = list(set(go_terms_propogated))
    return go_terms_propogated


def precision_recall_score(ref, pred):
    tp = len([val for val in ref if val in pred])
    fp = len([val for val in pred if val not in ref])
    fn = len([val for val in ref if val not in pred])
    if (tp + fp) == 0:
        precision = 0
    else:
        precision = float(tp)/(tp + fp)

    if (tp + fn) == 0:
        recall = 0
    else:
        recall = float(tp)/(tp + fn)
    return precision, recall


def prf_curve_single(test, pred, godag):
    threshold = np.linspace(0, 0.999, 100)
    new_test = test[(test == 0) | (test == 1)]
    new_pred = pred[new_test.index]
    precision = []
    recall = []
    for t in threshold:
        # print t
        p, r = calc_precision_recall(new_test, new_pred, t, godag)
        precision.append(p)
        recall.append(r)
    return precision, recall


def prf_curve_gene(test_array, pred_array):
    test_array[test_array < 0] = 0
    prots = test_array.index
    pre_gene = []
    rec_gene = []
    obo_file = "data/go-basic.obo"
    godag = GODag(obo_file)
    for prot in prots:
        pre, rec = prf_curve_single(test_array.loc[prot, :], pred_array.loc[prot, :], godag)
        pre_gene.append(pre)
        rec_gene.append(rec)

    pre_gene = pd.DataFrame(pre_gene, index=prots)
    rec_gene = pd.DataFrame(rec_gene, index=prots)
    return pre_gene, rec_gene


def precision_recall_fmax_mcc_term_centric(test_array, pred_array):
    goterms_all = test_array.columns
    threshold = np.linspace(0, 0.999, 100)
    prec_go = pd.DataFrame(0, index=test_array.columns, columns=threshold)
    reca_go = pd.DataFrame(0, index=test_array.columns, columns=threshold)
    f1_go = pd.DataFrame(0, index=test_array.columns, columns=threshold)
    mcc_go = pd.DataFrame(0, index=test_array.columns, columns=threshold)
    fmax_value_index = pd.DataFrame(0, index=test_array.columns, columns=['theshold', 'value'])
    prec_max = pd.DataFrame(0, index=test_array.columns, columns=['max'])
    reca_max = pd.DataFrame(0, index=test_array.columns, columns=['max'])
    mcc_max = pd.DataFrame(0, index=test_array.columns, columns=['max'])
    for t in threshold:
        for goterm in goterms_all:
            test_g = test_array.loc[:, goterm]
            pred_g = pred_array.loc[:, goterm]
            new_test_g = test_g[(test_g == 0) | (test_g == 1)]
            tmp = pred_g[(test_g == 0) | (test_g == 1)]
            new_pred_g = [binary(v, t) for v in tmp]
            # new_pred_log = pred_log.loc[:, goterm][(test_g == 0) | (test_g == 1)]
            # new_pred_g[new_pred_log] = 1
            # new_pred_g[-new_pred_log] = 0
            prec_go.loc[goterm, t] = precision_score(new_test_g, new_pred_g)
            reca_go.loc[goterm, t] = recall_score(new_test_g, new_pred_g)
            f1_go.loc[goterm, t] = f1_score(new_test_g, new_pred_g)
            mcc_go.loc[goterm, t] = matthews_corrcoef(new_test_g, new_pred_g)
            if f1_go.loc[goterm, t] > fmax_value_index.loc[goterm, 'value']:
                fmax_value_index.loc[goterm, 'value'] = f1_go.loc[goterm, t]
                fmax_value_index.loc[goterm, 'threshold'] = t
                prec_max.loc[goterm, 'max'] = prec_go.loc[goterm, t]
                reca_max.loc[goterm, 'max'] = reca_go.loc[goterm, t]
                mcc_max.loc[goterm, 'max'] = mcc_go.loc[goterm, t]
    fmax_go = fmax_value_index.loc[:, 'value']
    fmax_go.columns = ['max']
    return prec_max, reca_max, fmax_go, mcc_max


def fmax_vec(test, pred_prob):
    threshold = np.linspace(0, 0.999, 100)
    fmax = 0
    for t in threshold:
        pred = [binary(p, t) for p in pred_prob]
        f1 = f1_score(test, pred)
        if fmax < f1:
            fmax = f1
    return fmax


def precision_recall_f1_mcc_term_centric(test_array, pred_array):
    prec_go = pd.DataFrame(0, index=test_array.columns, columns=['precision'])
    reca_go = pd.DataFrame(0, index=test_array.columns, columns=['recall'])
    f1_go = pd.DataFrame(0, index=test_array.columns, columns=['f1'])
    mcc_go = pd.DataFrame(0, index=test_array.columns, columns=['mcc'])
    goterms_all = test_array.columns
    threshold = 0.5

    for goterm in goterms_all:
        test_g = test_array.loc[:, goterm]
        pred_g = pred_array.loc[:, goterm]
        new_test_g = test_g[(test_g == 0) | (test_g == 1)]
        tmp = pred_g[(test_g == 0) | (test_g == 1)]
        new_pred_g = [binary(v, threshold) for v in tmp]
        # new_pred_log = pred_log.loc[:, goterm][(test_g == 0) | (test_g == 1)]
        # new_pred_g[new_pred_log] = 1
        # new_pred_g[-new_pred_log] = 0
        prec_go.loc[goterm, 'precision'] = precision_score(new_test_g, new_pred_g)
        reca_go.loc[goterm, 'recall'] = recall_score(new_test_g, new_pred_g)
        f1_go.loc[goterm, 'f1'] = f1_score(new_test_g, new_pred_g)
        mcc_go.loc[goterm, 'mcc'] = matthews_corrcoef(new_test_g, new_pred_g)
    return prec_go, reca_go, f1_go, mcc_go


def calc_f1_score(pre, rec):
    f1_scores = pd.DataFrame(0, index=pre.index, columns=['F1_scores'])
    for term in pre.index:
        if (pre[term] * rec[term]) != 0:
            f1 = 2 * pre[term] * rec[term] /(pre[term] + rec[term])
        else:
            f1 = 0
        f1_scores.loc[term, 'F1_scores'] = f1
    return f1_scores


def mcc_go(test_array, pred_array):
    threshold = 0.5
    # obo_file = "data/go-basic.obo"
    # godag = GODag(obo_file)
    mcc = pd.DataFrame(0, index=test_array.columns, columns=['mcc'])
    goterms = test_array.columns
    pred_log = (pred_array >= threshold)
    for gid in goterms:
        test_g = test_array.loc[:, gid]
        pred_g = pred_array.loc[:, gid]
        new_test_g = test_g[(test_g == 0) | (test_g == 1)]
        new_pred_g = pred_g[(test_g == 0) | (test_g == 1)]
        new_pred_log = pred_log.loc[:, gid][(test_g == 0) | (test_g == 1)]
        new_pred_g[new_pred_log] = 1
        new_pred_g[-new_pred_log] = 0
        mcc.loc[gid, 'mcc'] = matthews_corrcoef(new_test_g, new_pred_g)

    return mcc


def get_leaf_terms(go_terms):
    obo_file = "data/go-basic.obo"
    godag = GODag(obo_file)
    leaf_terms = []
    for got in go_terms:
        rec = godag.query_term(got, verbose=False)
        a = rec.get_all_child_edges()
        b = list(a)
        tmp = [g for i in range(len(b)) for g in b[i][:]]
        offsprings = list(set(tmp))
        if got in offsprings:
            offsprings.remove(got)
        if not any(i in go_terms for i in offsprings):
            leaf_terms.append(got)

    return leaf_terms


def recal_precision_recall_term_centric_array(test_array, pred_array, nnet):
    output_path = nnet['output_path']
    category = nnet['category']
    pre_go, rec_go, f1_go, mcc_go = precision_recall_f1_mcc_term_centric(test_array, pred_array)
    pre_go.to_csv(join(output_path, category + '_precision_go.csv'), sep='\t', index=True)
    rec_go.to_csv(join(output_path, category + '_recall_go.csv'), sep='\t', index=True)
    f1_go.to_csv(join(output_path, category + '_f1_go.csv'), sep='\t', index=True)
    mcc_go.to_csv(join(output_path, category + '_mcc_go.csv'), sep='\t', index=True)
    pre_max, rec_max, f_max, mcc_max = precision_recall_fmax_mcc_term_centric(test_array, pred_array)
    pre_max.to_csv(join(output_path, category + '_precision_max.csv'), sep='\t', index=True)
    rec_max.to_csv(join(output_path, category + '_recall_max.csv'), sep='\t', index=True)
    f_max.to_csv(join(output_path, category + '_f1_max.csv'), sep='\t', index=True)
    mcc_max.to_csv(join(output_path, category + '_mcc_max.csv'), sep='\t', index=True)


def recal_precision_recall_term_centric(nnet, cv=0):
    output_path = nnet['output_path']
    category = nnet['category']
    goterm_filename = nnet['goterm_filename']
    filename_test_y_prefix = nnet['filename_test_y']
    filename_test_y = filename_test_y_prefix + '_' + str(cv) + '.csv'
    y_test = pd.read_csv(filename_test_y, sep='\t', header=0, index_col=0)
    all_proteins_test = [p.replace('"', '') for p in y_test.index]
    y_test.index = all_proteins_test
    goid = [[]] * 3
    # bp
    goterm_filename_bp = 'data/leaf_terms_bp.txt'
    goid_tmp = []
    with open(goterm_filename_bp, 'r') as f:
        files = f.readlines()
        for fi in files:
            tmp = fi[0:-1]
            tmp = tmp[0:2] + tmp[3:]
            goid_tmp.append(tmp)
    goid[0] = goid_tmp
    # cc
    goterm_filename_cc = 'data/leaf_terms_cc.txt'
    goid_tmp = []
    with open(goterm_filename_cc, 'r') as f:
        files = f.readlines()
        for fi in files:
            tmp = fi[0:-1]
            tmp = tmp[0:2] + tmp[3:]
            goid_tmp.append(tmp)
    goid[1] = goid_tmp
    # mf
    goterm_filename_mf = 'data/leaf_terms_mf.txt'
    goid_tmp = []
    with open(goterm_filename_mf, 'r') as f:
        files = f.readlines()
        for fi in files:
            tmp = fi[0:-1]
            tmp = tmp[0:2] + tmp[3:]
            goid_tmp.append(tmp)
    goid[2] = goid_tmp

    prediction_fn = category + '_prediction_' + str(cv) + '.csv'
    pred_array = pd.read_csv(join(output_path, prediction_fn), sep='\t', header=0, index_col=0)
    if category == 'all':
        cats = ['bp', 'cc', 'mf']
        for n in xrange(3):
            c = cats[n]
            goid_tmp = goid[n]
            y_test_tmp = y_test.loc[:, goid_tmp]
            pred_tmp = pred_array.loc[:, goid_tmp]
            pre_go, rec_go, f1_go, mcc_go = precision_recall_f1_mcc_term_centric(y_test_tmp, pred_tmp)
            pre_go.to_csv(join(output_path, c + '_leaf_precision_go_' + str(cv) + '.csv'),sep='\t', index=True)
            rec_go.to_csv(join(output_path, c + '_leaf_recall_go_' + str(cv) + '.csv'), sep='\t', index=True)
            f1_go.to_csv(join(output_path, c + '_leaf_f1_go_' + str(cv) + '.csv'), sep='\t', index=True)
            pre_max, rec_max, f_max, mcc_max = precision_recall_fmax_mcc_term_centric(y_test_tmp, pred_tmp)
            pre_max.to_csv(join(output_path, c + '_leaf_precision_max_' + str(cv) + '.csv'),sep='\t', index=True)
            rec_max.to_csv(join(output_path, c + '_leaf_recall_max_' + str(cv) + '.csv'), sep='\t', index=True)
            f_max.to_csv(join(output_path, c + '_leaf_f1_max_' + str(cv) + '.csv'), sep='\t', index=True)
    else:
        c = category
        if c == 'bp':
            n = 0
        elif c == 'cc':
            n = 1
        else:
            n = 2
        goid_tmp = goid[n]
        y_test_tmp = y_test.loc[:, goid_tmp]
        pred_tmp = pred_array.loc[:, goid_tmp]
        pre_go, rec_go, f1_go, mcc_go = precision_recall_f1_mcc_term_centric(y_test_tmp, pred_tmp)
        pre_go.to_csv(join(output_path, c + '_leaf_precision_go_' + str(cv) + '.csv'),sep='\t', index=True)
        rec_go.to_csv(join(output_path, c + '_leaf_recall_go_' + str(cv) + '.csv'), sep='\t', index=True)
        f1_go.to_csv(join(output_path, c + '_leaf_f1_go_' + str(cv) + '.csv'), sep='\t', index=True)
        pre_max, rec_max, f_max, mcc_max = precision_recall_fmax_mcc_term_centric(y_test_tmp, pred_tmp)
        pre_max.to_csv(join(output_path, c + '_leaf_precision_max_' + str(cv) + '.csv'),sep='\t', index=True)
        rec_max.to_csv(join(output_path, c + '_leaf_recall_max_' + str(cv) + '.csv'), sep='\t', index=True)
        f_max.to_csv(join(output_path, c + '_leaf_f1_max_' + str(cv) + '.csv'), sep='\t', index=True)


def binary(v, T=0.5):
    if v >= T:
        return 1
    else:
        return 0


def shuffle_list(a):
    b = copy.deepcopy(list(a))
    np.random.shuffle(b)
    return b


def load_test_data(branch_term):
    all_data_x_fn = 'data/all_data_X.csv'
    all_data_x = pd.read_csv(all_data_x_fn, sep='\t', header=0, index_col=0)
    all_proteins_train = [p.replace('"', '') for p in all_data_x.index]
    all_data_x.index = all_proteins_train
    test_fn = 'data/train_sets/' + branch_term[0:2] + branch_term[3:] + '_test.csv'
    test_y = pd.read_csv(test_fn, sep='\t', header=0, index_col=0)
    proteins_test = [p.replace('"', '') for p in test_y.index]
    test_y.index = proteins_test
    test_x = all_data_x.loc[test_y.index, :]
    return test_x, test_y


##########################################################################################################
# Performance measure
##########################################################################################################
def precision_recall_f1(test, pred):
    threshold = 0.5
    f_max = fmax_vec(test, pred)
    tmp = [binary(v, threshold) for v in pred]
    prec = precision_score(test, tmp) # break here
    reca = recall_score(test, tmp)
    f1 = f1_score(test, tmp)
    return prec, reca, f1, f_max


def mcc_calc(tp, tn, fp, fn):
    tp = np.int64(tp)
    tn = np.int64(tn)
    fp = np.int64(fp)
    fn = np.int64(fn)
    if ((tp + fp) == 0) | ((tp + fn) == 0) | ((tn + fp) == 0) | ((tn + fn) == 0):
        mcc = 0
    else:
        mcc = (tp * tn - fp * fn) / (np.sqrt(tp + fp) * np.sqrt(tp + fn) * np.sqrt(tn + fp) * np.sqrt(tn + fn))
    return mcc


def precision_recall_f1_single_term(test, pred):
    threshold = 0.5
    f_max = fmax_vec(test, pred)
    pred_t = np.empty_like(pred)
    pred_t[:] = pred
    pred_t[pred >= threshold] = 1
    pred_t[pred < threshold] = 0
    prec = precision_score(test, pred_t) # break here
    reca = recall_score(test, pred_t)
    f1 = f1_score(test, pred_t)
    cm = confusion_matrix(test, pred_t)
    tp = cm[1][1]
    fp = cm[1][0]
    fn = cm[0][1]
    tn = cm[0][0]
    mcc = mcc_calc(tp, tn, fp, fn)
    return prec, reca, f1, f_max, tp, fp, fn, tn, mcc


def f1_score_array(array_a, array_b):
    a = array_a.reshape(1, array_a.shape[0] * array_a.shape[1])
    b = array_b.reshape(1, array_b.shape[0] * array_b.shape[1])
    bb = b[a != np.inf]
    bb = [binary(i) for i in bb]
    aa = a[a != np.inf]
    f1 = f1_score(aa, bb)
    return f1


####################################################################################
# ID mapping, UniProt info dicts
####################################################################################
def save_id_mapping(dict, filename):
    with smart_open(filename, 'wb') as fp:
        pickle.dump(dict, fp)
        fp.flush()


def load_id_mapping(filename):
    with smart_open(filename, 'rb') as fp:
        dict_mapp = pickle.load(fp)
    return dict_mapp


def load_uniprot_info(filename):
    with smart_open(filename, 'rb') as fp:
        dict_mapp = pickle.load(fp)
    return dict_mapp


def gen_trans_uniprot_mapp():
    # Generate Transcrit-UniProt Mapping
    path = '/home/rui/Documents/PyCharmWorkplace/Experiments/ExpGTEx_Transcripts/'
    id_mapping_fn = 'data/transcript_uniprot_all.tab'
    mapping = pd.read_csv(join(path, id_mapping_fn), sep='\t', header=0, index_col=None)
    dict_map_all = dict()
    for i in mapping.index:
        tmp_u = mapping.loc[i, 'UniProtAC']
        tmp_e = mapping.loc[i, 'Transcript']
        tmp_e = tmp_e.replace('\'', '')
        if ',' in tmp_e:
            tmp_e = tmp_e.split(',')
        if tmp_u in dict_map_all.keys():
            tmp = dict_map_all[tmp_u.replace('\'', '')]
            if type(tmp) is str:
                tmp = [tmp]
                if type(tmp_e) is str:
                    tmp.append(tmp_e)
                else:
                    tmp.extend(tmp_e)
            else:
                if type(tmp_e) is str:
                    tmp.append(tmp_e)
                else:
                    tmp.extend(tmp_e)
            dict_map_all[tmp_u.replace('\'', '')] = tmp
        else:
            dict_map_all[tmp_u.replace('\'', '')] = tmp_e

    with smart_open(join(path, 'data/transcript_uniprot_mapping_p2.dict'), 'wb') as fp:
        pickle.dump(dict_map_all, fp)
        fp.flush()


####################################################################################
# Function Transfer
####################################################################################
def func_trans(term, label, prediction, test_proteins, train_pos_proteins, category='BP', evidence='exp'):
    pred_mat = form_pred_mat(label, prediction, test_proteins, train_pos_proteins)

    save_fn = 'data/human_' + category + '_' + evidence + '.tree'
    goa_tree = load_tree(save_fn)
    offspring = goa_tree.get_offsprings_annot(term)

    transfer_mat = pd.DataFrame(0, index=test_proteins, columns=offspring)
    for goterm in offspring:
        pos_anno = goa_tree[goterm].get_positive_annotations()
        in_train_pos = [p for p in pos_anno if p in pred_mat.columns]
        if len(in_train_pos) > 0:
            transfer_mat.loc[:, goterm] = np.mean(pred_mat.loc[:, in_train_pos], axis=1)
        else:
            transfer_mat.loc[:, goterm] = None
    return transfer_mat
    # Here we measure the performance in terms of GO-term centric metric


def form_pred_mat(label, prediction, test_proteins, train_pos_proteins):
    test_proteins = test_proteins[0]
    test_proteins.extend(test_proteins[1])
    pred_mat = pd.DataFrame(0, index=test_proteins, columns=train_pos_proteins)
    label_pos = label[0]
    label_neg = label[1]
    pred_pos = prediction[0]
    pred_neg = prediction[1]
    for label, i in zip(label_pos, xrange(len(label_pos))):
        tmp = label.split('_')
        pred_mat.loc[tmp[1], tmp[2]] = pred_pos[i]
    for label, i in zip(label_neg, xrange(len(label_neg))):
        tmp = label.split('_')
        pred_mat.loc[tmp[1], tmp[2]] = pred_neg[i]
    return pred_mat


def save_prediction(fn, func_mat):
    with open(fn, 'w') as f:
        for p in func_mat.index:
            for g in func_mat.columns:
                if ':' in g:
                    f.write('%s\t%s\t%.2f\n' % (p, g, func_mat.loc[p, g]))
                else:
                    f.write('%s\t%s\t%.2f\n' % (p, g[0:2] + ':' + g[2:], func_mat.loc[p, g]))


def multi_task_performance_measure(test_y, pred_y):
    num_task = test_y.shape[1]
    metrics = {}
    for n in xrange(num_task):
        protein_in_this_task = test_y.index[test_y.loc[:, test_y.columns[n]] != inf]
        index = [i for i in xrange(len(test_y.index)) if test_y.loc[test_y.index[i], test_y.columns[n]] != inf]
        test_y_t = test_y.loc[protein_in_this_task, test_y.columns[n]]
        pred_y_t = pred_y[index, n]
        precision, recall, f1, fmax, tp, fp, fn, tn, mcc = precision_recall_f1_single_term(test_y_t.values, pred_y_t)
        metrics[test_y.columns[n]] = [precision, recall, f1, fmax, tp, fp, fn, tn, mcc]
    return metrics


def save_prediction_final(fn, func_mat):
    with open(fn, 'w') as f:
        for p in func_mat.index:
            for g in func_mat.columns:
                if func_mat.loc[p, g] > 0.01:
                    f.write('%s\t%s\t%.2f\n' % (p, g, func_mat.loc[p, g]))


#########################################################################################
# Create Command for morecambe
##########################################################################################
def cmd_setup(category):
    branch_fn = 'data/sub/' + category + '_branches.txt'
    branches = []
    with open(branch_fn, 'r') as f:
        lines = f.readlines()
        for l in lines:
            branches.append(l[:-1])
    cmd_sge_fn = 'submit_mtdnn_' + category + '.sh'
    with open(cmd_sge_fn, 'wb') as f:
        f.write('#$ -S /bin/bash\n')
    for b in branches:
        # cmd_fn = generate_legion_cmd(b, category)
        cmd_fn = generate_morecambe_cmd(b, category)
        with open(cmd_sge_fn, 'a') as f:
            f.write('qsub ' + cmd_fn + ' > job_sub_mtdnn_' + b[0:2] + b[3:] + '_mtdnn\n')


def generate_morecambe_cmd(branch, category):
    branch_fn = 'data/sub/' + category + '_sub_' + branch[0:2] + branch[3:] + '.txt'
    lines = read_txt_list(branch_fn)
    num_task = len(lines)

    if num_task < 40:
        mem_alloc = 3.9
    elif num_task < 80:
        mem_alloc = 7.9
    elif num_task < 200:
        mem_alloc = 15.7
    elif num_task < 300:
        mem_alloc = 23.5
    elif num_task < 600:
        mem_alloc = 31.3
    else:
        mem_alloc = 39.1

    cmd_fn = 'cmd/' + branch[0:2] + branch[3:] + '_mtdnn.sh'
    with open(cmd_fn, 'wb') as f:
        f.write('#$ -l tmem=' + str(mem_alloc) + 'G\n')
        f.write('#$ -l h_vmem=' + str(mem_alloc) + 'G\n')
        f.write('#$ -N ' + branch[0:2] + branch[3:] + '\n')
        f.write('#$ -l h_rt=1000:0:0\n')
        f.write('#$ -l scr=1G\n')
        f.write('#$ -R y\n')
        f.write('#$ -S /bin/bash\n')
        f.write('#$ -wd /cluster/scratch8/maseq_human_raw/PythonWorkspace/mtdnn\n')
        f.write('#$ -t 1-15\n')
        f.write('\n')
        f.write('rm -rf /scratch0/ruifax01/' + branch[0:2] + branch[3:] + '/\n')
        f.write('mkdir -p /scratch0/ruifax01/' + branch[0:2] + branch[3:] + '/\n')
        f.write('export PATH=/SAN/bioinf/ifpred/usr/anaconda2/bin:$PATH\n')
        f.write("THEANO_FLAGS='base_compiledir=/scratch0/ruifax01/" + branch[0:2] + branch[3:] + "/.theano' python mtdnn_train.py -b " +
                branch + ' -s $SGE_TASK_ID -l log/' + branch[0:2] + branch[3:] +
                '_setting_$SGE_TASK_ID.log\n')
        f.write('rm -rf /scratch0/ruifax01/' + branch[0:2] + branch[3:] + '/\n')
    return cmd_fn


##################################################################################################
# branches management
#
##################################################################################################
def create_branches():
    mf_goterm = 'GO:0003674'
    bp_goterm = 'GO:0008150'
    cc_goterm = 'GO:0005575'

    obo_file = "data/gene_ontology_edit.obo.2015-02-03"
    g = GODag(obo_file)
    rec = g.query_term(mf_goterm, verbose=False)
    mf_branch_terms = []
    for go in rec.children:
        mf_branch_terms.append(go.id)

    rec = g.query_term(bp_goterm, verbose=False)
    bp_branch_terms = []
    for go in rec.children:
        bp_branch_terms.append(go.id)

    rec = g.query_term(cc_goterm, verbose=False)
    cc_branch_terms = []
    for go in rec.children:
        cc_branch_terms.append(go.id)

    mf_branch_terms_fn = 'data/sub/mf_branches.txt'
    bp_branch_terms_fn = 'data/sub/bp_branches.txt'
    cc_branch_terms_fn = 'data/sub/cc_branches.txt'
    write_txt_list(mf_branch_terms_fn, mf_branch_terms)
    write_txt_list(bp_branch_terms_fn, bp_branch_terms)
    write_txt_list(cc_branch_terms_fn, cc_branch_terms)

    sub_branches(mf_branch_terms, 'mf')
    sub_branches(bp_branch_terms, 'bp')
    sub_branches(cc_branch_terms, 'cc')


def sub_branch_targets(branchTerm, targetTerms):
    obo_file = "data/gene_ontology_edit.obo.2015-02-03"
    g = GODag(obo_file)
    rec = g.query_term(branchTerm, verbose=False)
    children = []
    for go in rec.children:
        children.append(go.id)
    n = 0
    # print(children)
    targets = []
    t = branchTerm[0:2] + branchTerm[3:]
    if t in targetTerms:
        targets.append(t)
    while n < len(children):
        tmp = children[n][0:2] + children[n][3:]
        if tmp in targetTerms:
            targets.append(tmp)
        rec = g.query_term(children[n], verbose=False)
        for c in rec.children:
            #print(c.id)
            children.append(c.id)
        n += 1
    return list(set(targets))


def sub_branches(branchTerms, type):
    fn = 'data/all_data_Y.csv'
    all_data_Y = pd.read_csv(fn, sep='\t', header=0, index_col=0)
    all_proteins_train = [p.replace('"', '') for p in all_data_Y.index]
    all_data_Y.index = all_proteins_train
    targetTerms = all_data_Y.columns

    for b in branchTerms:
        fn_sub = 'data/sub/' + type + '_sub_' + b[0:2] + b[3:] + '.txt'
        targets = sub_branch_targets(b, targetTerms)
        if len(targets) > 1:
            with open(fn_sub, 'w') as fpt:
                for t in targets:
                    fpt.write(t + '\n')


def read_branches(type):
    fn_b = 'data/sub/' + type + '_branches.txt'
    branches = read_txt_list(fn_b)
    targets = []
    for b in branches:
        fn_sub = 'data/sub/' + type + '_sub_' + b[0:2] + b[3:] + '.txt'
        if exists(fn_sub):
            tmp = read_txt_list(fn_sub)
            print ("branch: " + b + ", size: " + str(len(tmp)))
            targets.extend(tmp)
    return targets


def check_duplicate_goterms():
    bp_all = read_txt_list('data/biological_process_all.txt')
    mf_all = read_txt_list('data/molecular_function_all.txt')
    cc_all = read_txt_list('data/cellular_component_all.txt')

    targets_bp = read_branches('bp')
    targets_mf = read_branches('mf')
    targets_cc = read_branches('cc')

    print ("bp list has %d GO terms, branch list has % d GO terms." % (len(bp_all), len(targets_bp)))
    print ("mf list has %d GO terms, branch list has % d GO terms." % (len(mf_all), len(targets_mf)))
    print ("cc list has %d GO terms, branch list has % d GO terms." % (len(cc_all), len(targets_cc)))

    print ("bp list has %d GO terms, branch list has % d GO terms." % (len(bp_all),
                                                                      len(list(set(targets_bp)))))
    print ("mf list has %d GO terms, branch list has % d GO terms." % (len(mf_all),
                                                                      len(list(set(targets_mf)))))
    print ("cc list has %d GO terms, branch list has % d GO terms." % (len(cc_all),
                                                                      len(list(set(targets_cc)))))


def refine_branches():
    type = 'mf'
    fn_b = 'data/sub/' + type + '_branches.txt'
    mf_branches = read_txt_list(fn_b)
    tmp = [b for b in mf_branches ]
    for b in mf_branches:
        fn_sub = 'data/sub/' + type + '_sub_' + b[0:2] + b[3:] + '.txt'
        if not exists(fn_sub):
            tmp.pop(tmp.index(b))
    write_txt_list(fn_b, tmp)

    type = 'bp'
    fn_b = 'data/sub/' + type + '_branches.txt'
    bp_branches = read_txt_list(fn_b)
    tmp = [b for b in bp_branches]
    for b in bp_branches:
        fn_sub = 'data/sub/' + type + '_sub_' + b[0:2] + b[3:] + '.txt'
        if not exists(fn_sub):
            tmp.pop(tmp.index(b))
    write_txt_list(fn_b, tmp)

    type = 'cc'
    fn_b = 'data/sub/' + type + '_branches.txt'
    cc_branches = read_txt_list(fn_b)
    tmp = [b for b in cc_branches]
    for b in cc_branches:
        fn_sub = 'data/sub/' + type + '_sub_' + b[0:2] + b[3:] + '.txt'
        if not exists(fn_sub):
            tmp.pop(tmp.index(b))
    write_txt_list(fn_b, tmp)


####################################################################################################
# Create train, valid, test sets for all branches
####################################################################################################
def create_train_valid_test_sets(branch, type):
    fn_sub = 'data/sub/' + type + '_sub_' + branch[0:2] + branch[3:] + '.txt'
    if exists(fn_sub):
        goterms = read_txt_list(fn_sub)
    else:
        return 0

    train_index = []
    valid_index = []
    test_index = []
    for goterm in goterms:
        train_fn = 'data/cv_ffpred_data/' + goterm + '_train_0.txt'
        train_index.extend(read_txt_list(train_fn))
        valid_fn = 'data/cv_ffpred_data/' + goterm + '_test_0.txt'
        valid_index.extend(read_txt_list(valid_fn))
        test_fn = 'data/holdout_ffpred_data/' + goterm + '_test_holdout.txt'
        test_index.extend(read_txt_list(test_fn))

    train_index = list(set(train_index))
    valid_index = list(set(valid_index))
    test_index = list(set(test_index))
    train_df = pd.DataFrame(np.inf, index=train_index, columns=goterms)
    valid_df = pd.DataFrame(np.inf, index=valid_index, columns=goterms)
    test_df = pd.DataFrame(np.inf, index=test_index, columns=goterms)
    all_data_y = pd.read_csv('data/all_data_Y.csv', sep='\t', header=0, index_col=0)
    all_proteins = [p.replace('"', '') for p in all_data_y.index]
    all_data_y.index = all_proteins
    for goterm in goterms:
        train_fn = 'data/cv_ffpred_data/' + goterm + '_train_0.txt'
        train_index = read_txt_list(train_fn)
        train_df.loc[train_index, goterm] = all_data_y.loc[train_index, goterm]
        valid_fn = 'data/cv_ffpred_data/' + goterm + '_test_0.txt'
        valid_index = read_txt_list(valid_fn)
        valid_df.loc[valid_index, goterm] = all_data_y.loc[valid_index, goterm]
        test_fn = 'data/holdout_ffpred_data/' + goterm + '_test_holdout.txt'
        test_index = read_txt_list(test_fn)
        test_df.loc[test_index, goterm] = all_data_y.loc[test_index, goterm]

    train_df.to_csv('data/train_sets/' + branch[0:2] + branch[3:] + '_train.csv', sep='\t')
    valid_df.to_csv('data/train_sets/' + branch[0:2] + branch[3:] + '_valid.csv', sep='\t')
    test_df.to_csv('data/train_sets/' + branch[0:2] + branch[3:] + '_test.csv', sep='\t')


def create_branches_train_valid_test_sets():
    type = 'mf'
    fn_b = 'data/sub/' + type + '_branches.txt'
    mf_branches = read_txt_list(fn_b)
    for mfb in mf_branches:
        create_train_valid_test_sets(mfb, type)

    type = 'bp'
    fn_b = 'data/sub/' + type + '_branches.txt'
    mf_branches = read_txt_list(fn_b)
    for mfb in mf_branches:
        create_train_valid_test_sets(mfb, type)

    type = 'cc'
    fn_b = 'data/sub/' + type + '_branches.txt'
    mf_branches = read_txt_list(fn_b)
    for mfb in mf_branches:
        create_train_valid_test_sets(mfb, type)


##############################################################################################
# Final models
#
##############################################################################################
def check_finished_branches():
    bp_branches = read_txt_list('data/sub/bp_branches.txt')
    mf_branches = read_txt_list('data/sub/mf_branches.txt')
    cc_branches = read_txt_list('data/sub/cc_branches.txt')

    bp_finished_branches = []
    for b in bp_branches:
        if finished(b, 'bp'):
            bp_finished_branches.append(b)

    mf_finished_branches = []
    for b in mf_branches:
        if finished(b, 'mf'):
            mf_finished_branches.append(b)

    cc_finished_branches = []
    for b in cc_branches:
        if finished(b, 'cc'):
            cc_finished_branches.append(b)

    write_txt_list('data/bp_finished_branches.txt', bp_finished_branches)
    write_txt_list('data/mf_finished_branches.txt', mf_finished_branches)
    write_txt_list('data/cc_finished_branches.txt', cc_finished_branches)


def finished(branch_term, category):
    num_finished_setting = 0
    predict_fn = branch_term[0:2] + branch_term[3:] + '_' + category + '.predict'
    metrics_fn = branch_term[0:2] + branch_term[3:] + '_' + category + '.metrics'
    total_settings = 15
    for s in xrange(total_settings):
        path = 'outputs/' + branch_term[0:2] + branch_term[3:] + '_setting_' + str(s)
        if exists(join(path, predict_fn)) & exists(join(path, metrics_fn)):
            num_finished_setting += 1
    if num_finished_setting == total_settings:
        return True
    else:
        return False


def get_final_model():
    check_finished_branches()
    bp_finished_branches = read_txt_list('data/bp_finished_branches.txt')
    mf_finished_branches = read_txt_list('data/mf_finished_branches.txt')
    cc_finished_branches = read_txt_list('data/cc_finished_branches.txt')

    total_settings = 15
    for b in bp_finished_branches:
        metrics_fn = b[0:2] + b[3:] + '_bp.metrics'
        max_f1 = 0
        best_setting = 0
        for s in xrange(total_settings):
            path = 'outputs/' + b[0:2] + b[3:] + '_setting_' + str(s)
            with smart_open(join(path, metrics_fn), 'r') as f:
                metrics = pickle.load(f)
            f1_s = 0
            for m in metrics.keys():
                f1_s += metrics[m][2]
            if max_f1 < f1_s:
                max_f1 = f1_s
                best_setting = s
        configs_fn = 'configs/' + b[0:2] + b[3:] + '_mtdnn_setting_' + str(best_setting) + '.dict'
        with smart_open(configs_fn, 'r') as f:
            configs = pickle.load(f)
        configs_final_fn = 'configs/' + b[0:2] + b[3:] + '_mtdnn_final.dict'
        configs['model_path'] = 'models/final/' + b[0:2] + b[3:] + '/'
        configs['output_path'] = 'outputs/final/' + b[0:2] + b[3:]
        print ('%s\t%d' % (b, best_setting))
        with smart_open(configs_final_fn, 'wb') as fp:
            pickle.dump(configs, fp)
            fp.flush()

    for b in mf_finished_branches:
        metrics_fn = b[0:2] + b[3:] + '_mf.metrics'
        max_f1 = 0
        best_setting = 0
        for s in xrange(total_settings):
            path = 'outputs/' + b[0:2] + b[3:] + '_setting_' + str(s)
            with smart_open(join(path, metrics_fn), 'r') as f:
                metrics = pickle.load(f)
            f1_s = 0
            for m in metrics.keys():
                f1_s += metrics[m][2]
            if max_f1 < f1_s:
                max_f1 = f1_s
                best_setting = s
        configs_fn = 'configs/' + b[0:2] + b[3:] + '_mtdnn_setting_' + str(best_setting) + '.dict'
        with smart_open(configs_fn, 'r') as f:
            configs = pickle.load(f)
        configs_final_fn = 'configs/' + b[0:2] + b[3:] + '_mtdnn_final.dict'
        configs['model_path'] = 'models/final/' + b[0:2] + b[3:] + '/'
        configs['output_path'] = 'outputs/final/' + b[0:2] + b[3:]
        print ('%s\t%d' % (b, best_setting))
        with smart_open(configs_final_fn, 'wb') as fp:
            pickle.dump(configs, fp)
            fp.flush()

    for b in cc_finished_branches:
        metrics_fn = b[0:2] + b[3:] + '_cc.metrics'
        max_f1 = 0
        best_setting = 0
        for s in xrange(total_settings):
            path = 'outputs/' + b[0:2] + b[3:] + '_setting_' + str(s)
            with smart_open(join(path, metrics_fn), 'r') as f:
                metrics = pickle.load(f)
            f1_s = 0
            for m in metrics.keys():
                f1_s += metrics[m][2]
            if max_f1 < f1_s:
                max_f1 = f1_s
                best_setting = s
        configs_fn = 'configs/' + b[0:2] + b[3:] + '_mtdnn_setting_' + str(best_setting) + '.dict'
        with smart_open(configs_fn, 'r') as f:
            configs = pickle.load(f)
        configs_final_fn = 'configs/' + b[0:2] + b[3:] + '_mtdnn_final.dict'
        configs['model_path'] = 'models/final/' + b[0:2] + b[3:] + '/'
        configs['output_path'] = 'outputs/final/' + b[0:2] + b[3:]
        print ('%s\t%d' % (b, best_setting))
        with smart_open(configs_final_fn, 'wb') as fp:
            pickle.dump(configs, fp)
            fp.flush()


def copy_final_models():
    model_chosen = []
    with open('data/model_chosen', 'r') as f:
        lines = f.readlines()
        for l in lines:
            model_chosen.append(l[:-1])
    for m in model_chosen:
        tmp = m.split('\t')
        dest = 'models/final/' + tmp[0][0:2] + tmp[0][3:] + '/'
        if not exists(dest):
            makedirs(dest)
        source = 'models/' + tmp[0][0:2] + tmp[0][3:] + '_setting_' + tmp[1] + '/mtdnn.model.*'
        cmd = 'cp ' + source + ' ' + dest
        system(cmd)


def cmd_setup_final():
    bp_finished_branches = read_txt_list('data/bp_finished_branches.txt')
    mf_finished_branches = read_txt_list('data/mf_finished_branches.txt')
    cc_finished_branches = read_txt_list('data/cc_finished_branches.txt')
    cmd_sge_fn = 'submit_mtdnn_final.sh'
    with open(cmd_sge_fn, 'w') as f:
        f.write('#!/usr/bin/bash\n')
    for b in bp_finished_branches:
        cmd_fn = generate_morecambe_final_cmd(b, 'bp')
        with open(cmd_sge_fn, 'a') as f:
            f.write('qsub ' + cmd_fn + ' > job_sub_mtdnn_' + b[0:2] + b[3:] + '_mtdnn\n')
    for b in mf_finished_branches:
        cmd_fn = generate_morecambe_final_cmd(b, 'mf')
        with open(cmd_sge_fn, 'a') as f:
            f.write('qsub ' + cmd_fn + ' > job_sub_mtdnn_' + b[0:2] + b[3:] + '_mtdnn\n')
    for b in cc_finished_branches:
        cmd_fn = generate_morecambe_final_cmd(b, 'cc')
        with open(cmd_sge_fn, 'a') as f:
            f.write('qsub ' + cmd_fn + ' > job_sub_mtdnn_' + b[0:2] + b[3:] + '_mtdnn\n')


def generate_morecambe_final_cmd(branch, category):
    branch_fn = 'data/sub/' + category + '_sub_' + branch[0:2] + branch[3:] + '.txt'
    with open(branch_fn, 'r') as f:
        lines = f.readlines()
        num_task = len(lines)

    if num_task < 40:
        mem_alloc = 3.9
    elif num_task < 80:
        mem_alloc = 7.9
    elif num_task < 200:
        mem_alloc = 15.7
    elif num_task < 300:
        mem_alloc = 23.5
    elif num_task < 600:
        mem_alloc = 31.3
    else:
        mem_alloc = 39.1

    cmd_fn = 'cmd/' + branch[0:2] + branch[3:] + '_mtdnn_final.sh'
    with open(cmd_fn, 'wb') as f:
        f.write('#$ -l tmem=' + str(mem_alloc) + 'G\n')
        f.write('#$ -l h_vmem=' + str(mem_alloc) + 'G\n')
        f.write('#$ -N ' + branch[0:2] + branch[3:] + '\n')
        f.write('#$ -l h_rt=1000:0:0\n')
        f.write('#$ -l scr=1G\n')
        f.write('#$ -R y\n')
        f.write('#$ -S /bin/bash\n')
        f.write('#$ -wd /cluster/scratch8/maseq_human_raw/PythonWorkspace/mtdnn\n')
        f.write('\n')
        f.write('rm -rf /scratch0/ruifax01/' + branch[0:2] + branch[3:] + '/\n')
        f.write('mkdir -p /scratch0/ruifax01/' + branch[0:2] + branch[3:] + '/\n')
        f.write('export PATH=/SAN/bioinf/ifpred/usr/anaconda2/bin:$PATH\n')
        f.write('python mtdnn_predict.py -b ' + branch + ' -m train -i data/all_data_X.csv -l log/' +
                branch[0:2] + branch[3:] + '_mtdnn_final.log')
    return cmd_fn


def cmd_setup_final_cafa3():
    bp_finished_branches = read_txt_list('data/bp_finished_branches.txt')
    mf_finished_branches = read_txt_list('data/mf_finished_branches.txt')
    cc_finished_branches = read_txt_list('data/cc_finished_branches.txt')
    cmd_sge_fn = 'submit_mtdnn_final_cafa3.sh'
    with open(cmd_sge_fn, 'w') as f:
        f.write('#!/usr/bin/bash\n')
    for b in bp_finished_branches:
        cmd_fn = generate_morecambe_final_cmd_cafa3(b, 'bp')
        with open(cmd_sge_fn, 'a') as f:
            f.write('qsub ' + cmd_fn + ' > job_sub_mtdnn_' + b[0:2] + b[3:] + '_mtdnn\n')
    for b in mf_finished_branches:
        cmd_fn = generate_morecambe_final_cmd_cafa3(b, 'mf')
        with open(cmd_sge_fn, 'a') as f:
            f.write('qsub ' + cmd_fn + ' > job_sub_mtdnn_' + b[0:2] + b[3:] + '_mtdnn\n')
    for b in cc_finished_branches:
        cmd_fn = generate_morecambe_final_cmd_cafa3(b, 'cc')
        with open(cmd_sge_fn, 'a') as f:
            f.write('qsub ' + cmd_fn + ' > job_sub_mtdnn_' + b[0:2] + b[3:] + '_mtdnn\n')


def generate_morecambe_final_cmd_cafa3(branch, category):
    branch_fn = 'data/sub/' + category + '_sub_' + branch[0:2] + branch[3:] + '.txt'
    with open(branch_fn, 'r') as f:
        lines = f.readlines()
        num_task = len(lines)

    if num_task < 40:
        mem_alloc = 3.9
    elif num_task < 80:
        mem_alloc = 7.9
    elif num_task < 160:
        mem_alloc = 15.7
    elif num_task < 300:
        mem_alloc = 23.5
    elif num_task < 600:
        mem_alloc = 31.3
    else:
        mem_alloc = 39.1

    cmd_fn = 'cmd/' + branch[0:2] + branch[3:] + '_mtdnn_final_cafa3.sh'
    with open(cmd_fn, 'wb') as f:
        f.write('#$ -l tmem=' + str(mem_alloc) + 'G\n')
        f.write('#$ -l h_vmem=' + str(mem_alloc) + 'G\n')
        f.write('#$ -N ' + branch[0:2] + branch[3:] + '\n')
        f.write('#$ -l h_rt=1000:0:0\n')
        f.write('#$ -l scr=1G\n')
        f.write('#$ -R y\n')
        f.write('#$ -S /bin/bash\n')
        f.write('#$ -wd /cluster/scratch8/maseq_human_raw/PythonWorkspace/mtdnn\n')
        f.write('\n')
        f.write('rm -rf /scratch0/ruifax01/' + branch[0:2] + branch[3:] + '/\n')
        f.write('mkdir -p /scratch0/ruifax01/' + branch[0:2] + branch[3:] + '/\n')
        f.write('export PATH=/SAN/bioinf/ifpred/usr/anaconda2/bin:$PATH\n')
        f.write('python mtdnn_predict.py -b ' + branch + ' -m train -i data/all_features_cafa3.csv -l log/' +
                branch[0:2] + branch[3:] + '_mtdnn_final_cafa3.log')
    return cmd_fn


###############################################################################################
# Overlapping GO terms need to be combined by a logistic regression
###############################################################################################
def check_final_finished_jobs():
    bp_branches = read_txt_list('data/sub/bp_branches.txt')
    mf_branches = read_txt_list('data/sub/mf_branches.txt')
    cc_branches = read_txt_list('data/sub/cc_branches.txt')
    all_done = True
    for bpb in bp_branches:
        fn = 'outputs/final/' + bpb[0:2] + bpb[3:] + '/' + bpb[0:2] + bpb[3:] + '_final.predict'
        if not exists(fn):
            all_done = False
            print ("%s does not finish yet! %s" % (bpb, 'BP'))
    for mfb in mf_branches:
        fn = 'outputs/final/' + mfb[0:2] + mfb[3:] + '/' + mfb[0:2] + mfb[3:] + '_final.predict'
        if not exists(fn):
            all_done = False
            print ("%s does not finish yet! %s" % (mfb, 'MF'))
    for ccb in cc_branches:
        fn = 'outputs/final/' + ccb[0:2] + ccb[3:] + '/' + ccb[0:2] + ccb[3:] + '_final.predict'
        if not exists(fn):
            all_done = False
            print ("%s does not finish yet! %s" % (ccb, 'CC'))
    if all_done:
        print ("All final jobs have finished!")
    return all_done


def collect_all_prediction():
    if not check_final_finished_jobs():
        print ("There are unfinished jobs!")
        sys.exit(-1)

    bp_branches = read_txt_list('data/sub/bp_branches.txt')
    mf_branches = read_txt_list('data/sub/mf_branches.txt')
    cc_branches = read_txt_list('data/sub/cc_branches.txt')

    goterms = []
    for bpb in bp_branches:
        fn = 'data/sub/bp_sub_' + bpb[0:2] + bpb[3:] + '.txt'
        tmp = read_txt_list(fn)
        for b in tmp:
            goterms.append(bpb + '_' + b)
    for mfb in mf_branches:
        fn = 'data/sub/mf_sub_' + mfb[0:2] + mfb[3:] + '.txt'
        tmp = read_txt_list(fn)
        for b in tmp:
            goterms.append(mfb + '_' + b)
    for ccb in cc_branches:
        fn = 'data/sub/cc_sub_' + ccb[0:2] + ccb[3:] + '.txt'
        tmp = read_txt_list(fn)
        for b in tmp:
            goterms.append(ccb + '_' + b)

    fn_x = 'data/all_data_X.csv'
    data_x = pd.read_csv(fn_x, sep='\t', header=0, index_col=0)
    all_proteins = [p.replace('"', '') for p in data_x.index]
    data_x.index = all_proteins

    all_predict = pd.DataFrame(0, index=all_proteins, columns=goterms)
    for bpb in bp_branches:
        fn = 'outputs/final/' + bpb[0:2] + bpb[3:] + '/' + bpb[0:2] + bpb[3:] + '_final.predict'
        lines = read_txt_list(fn)
        print ("%s\t%d" % (bpb, len(lines)))
        for l in lines:
            tmp = l.split('\t')
            all_predict.loc[tmp[0], bpb + '_' + tmp[1]] = float(tmp[2])
    all_predict.to_csv('data/all_prediction.csv', sep='\t')
    for mfb in mf_branches:
        fn = 'outputs/final/' + mfb[0:2] + mfb[3:] + '/' + mfb[0:2] + mfb[3:] + '_final.predict'
        lines = read_txt_list(fn)
        print ("%s\t%d" % (mfb, len(lines)))
        for l in lines:
            tmp = l.split('\t')
            all_predict.loc[tmp[0], mfb + '_' + tmp[1]] = float(tmp[2])
    all_predict.to_csv('data/all_prediction.csv', sep='\t')
    for ccb in cc_branches:
        fn = 'outputs/final/' + ccb[0:2] + ccb[3:] + '/' + ccb[0:2] + ccb[3:] + '_final.predict'
        lines = read_txt_list(fn)
        print ("%s\t%d" % (ccb, len(lines)))
        for l in lines:
            tmp = l.split('\t')
            all_predict.loc[tmp[0], ccb + '_' + tmp[1]] = float(tmp[2])
    all_predict.to_csv('data/all_prediction.csv', sep='\t')


def collect_all_prediction_cafa3():
    if not check_final_finished_jobs():
        print ("There are unfinished jobs!")
        sys.exit(-1)

    bp_branches = read_txt_list('data/sub/bp_branches.txt')
    mf_branches = read_txt_list('data/sub/mf_branches.txt')
    cc_branches = read_txt_list('data/sub/cc_branches.txt')

    goterms = []
    for bpb in bp_branches:
        fn = 'data/sub/bp_sub_' + bpb[0:2] + bpb[3:] + '.txt'
        tmp = read_txt_list(fn)
        for b in tmp:
            goterms.append(bpb + '_' + b)
    for mfb in mf_branches:
        fn = 'data/sub/mf_sub_' + mfb[0:2] + mfb[3:] + '.txt'
        tmp = read_txt_list(fn)
        for b in tmp:
            goterms.append(mfb + '_' + b)
    for ccb in cc_branches:
        fn = 'data/sub/cc_sub_' + ccb[0:2] + ccb[3:] + '.txt'
        tmp = read_txt_list(fn)
        for b in tmp:
            goterms.append(ccb + '_' + b)

    fn_x = 'data/all_features_cafa3.csv'
    data_x = pd.read_csv(fn_x, sep='\t', header=0, index_col=0)
    all_proteins = [p.replace('"', '') for p in data_x.index]
    data_x.index = all_proteins

    all_predict = pd.DataFrame(0, index=all_proteins, columns=goterms)
    for bpb in bp_branches:
        fn = 'outputs/final/' + bpb[0:2] + bpb[3:] + '/' + bpb[0:2] + bpb[3:] + '_final.predict'
        lines = read_txt_list(fn)
        print ("%s\t%d" % (bpb, len(lines)))
        for l in lines:
            tmp = l.split('\t')
            all_predict.loc[tmp[0], bpb + '_' + tmp[1][0:2] + tmp[1][3:]] = float(tmp[2])
    all_predict.to_csv('data/all_prediction_cafa3.csv', sep='\t')
    for mfb in mf_branches:
        fn = 'outputs/final/' + mfb[0:2] + mfb[3:] + '/' + mfb[0:2] + mfb[3:] + '_final.predict'
        lines = read_txt_list(fn)
        print ("%s\t%d" % (mfb, len(lines)))
        for l in lines:
            tmp = l.split('\t')
            all_predict.loc[tmp[0], mfb + '_' + tmp[1][0:2] + tmp[1][3:]] = float(tmp[2])
    all_predict.to_csv('data/all_prediction_cafa3.csv', sep='\t')
    for ccb in cc_branches:
        fn = 'outputs/final/' + ccb[0:2] + ccb[3:] + '/' + ccb[0:2] + ccb[3:] + '_final.predict'
        lines = read_txt_list(fn)
        print ("%s\t%d" % (ccb, len(lines)))
        for l in lines:
            tmp = l.split('\t')
            all_predict.loc[tmp[0], ccb + '_' + tmp[1][0:2] + tmp[1][3:]] = float(tmp[2])
    all_predict.to_csv('data/all_prediction_cafa3.csv', sep='\t')


def generate_uniprot_dict(uniprot_fn):
    fh_uniprot = open(uniprot_fn, 'r')
    uniprot_dict = dict()
    cafaid = read_txt_list('data/cafa2_targets.cafaid')
    uniprot_id = read_txt_list('data/cafa2_targets.id')
    for rec in sp.parse(fh_uniprot):
        if rec.entry_name in uniprot_id:
            node = [cafaid[uniprot_id.index(rec.entry_name)], rec.accessions]
        else:
            node = ['NOT_CAFA_TARGET', rec.accessions]
        uniprot_dict[rec.entry_name] = node
    fh_uniprot.close()
    timestamp = uniprot_fn.split('.')[-1]
    with open('data/uniprot_mapping.' + timestamp + '.dict', 'w') as f:
        pickle.dump(uniprot_dict, f)
        f.flush()


def create_benchmark(t1_goa, t2_goa, go_bob, category):
    '''
        t1_goa: GOA file path for early time, the time mark should in the end separated by '.'
        t2_goa: GOA file path for recent time, the time mark should in the end separated by '.'
        for example: gene_association.goa_human.2015_2
    '''

    intm_t1 = t1_goa + '_exp.tree'
    time_mark_t1 = t1_goa.split('.')[-1]
    intm_t2 = t2_goa + '_exp.tree'
    time_mark_t2 = t2_goa.split('.')[-1]
    if not exists(intm_t1):
        create_go_tree(go_bob, t1_goa, intm_t1, 'exp')
    if not exists(intm_t2):
        create_go_tree(go_bob, t2_goa, intm_t2, 'exp')

    if category == 'P':
        branches = read_txt_list('data/sub/bp_branches.txt')
        fn_prefix = 'data/sub/bp_sub_'
    elif category == 'F':
        branches = read_txt_list('data/sub/mf_branches.txt')
        fn_prefix = 'data/sub/mf_sub_'
    elif category == 'C':
        branches = read_txt_list('data/sub/cc_branches.txt')
        fn_prefix = 'data/sub/cc_sub_'
    else:
        print ("No such category!")
        sys.exit(-1)

    vocabulary = []
    for b in branches:
        fn = fn_prefix + b[0:2] + b[3:] + '.txt'
        vocabulary.extend(read_txt_list(fn))

    go_tree_t1 = load_tree(intm_t1)
    go_tree_t2 = load_tree(intm_t2)
    bench_fn = 'data/benchmark.' + time_mark_t1 + '.' + time_mark_t2 + '.' + category + '.human_goa'
    benchmark_set = []
    with open(bench_fn, 'w') as f:
        for goterm in go_tree_t2.tree.keys():
            if goterm in go_tree_t1.tree.keys():
                pos_t1 = go_tree_t1.get_positive_annotations(goterm)
                pos_t2 = go_tree_t2.get_positive_annotations(goterm)
                for p in pos_t2:
                    if p not in pos_t1:
                        benchmark_set.append(p)
                        f.write('%s\t%s\n' % (p, goterm))
            else:
                print ("%s has been removed from new Gene Ontology!" % goterm)
    benchmark_set = list(set(benchmark_set))
    benchmark_set_fn = 'data/benchmark_set.' + time_mark_t1 + '.' + time_mark_t2 + '.' + category + '.prot'
    write_txt_list(benchmark_set_fn, benchmark_set)
    return bench_fn


def reformat(fn):
    uniprot_fn = 'data/uniprot_mapping.2017_1.dict'
    with open(uniprot_fn, 'r') as f:
        uniprot_map = pickle.load(f)

    with open(fn, 'r') as fr:
        with open(fn + '.reformat', 'w') as fw:
            for l in fr:
                tmp = l[:-1].split('\t')
                cafaid = find_cafa_id(tmp[0], uniprot_map)
                text = cafaid
                if (text != 'NOT_CAFA_TARGET') & (text is not None):
                    for t in tmp[1:]:
                        text += '\t'
                        text += t
                    fw.write(text + '\n')


def find_cafa_id(uniprot_ac, uniprot_map):
    for k in uniprot_map.keys():
        if uniprot_ac in uniprot_map[k][1]:
            return uniprot_map[k][0]


if __name__ == "__main__":
    # create_benchmark('data/gene_association.goa_human.2015_2', 'data/gene_association.goa_human.2017_1',
    #                  'data/gene_ontology_edit.obo.2016-12-02')
    # generate_uniprot_dict('data/uniprot_sprot_human.dat.2015_2')
    collect_all_prediction_cafa3()


# def mcc_go_singleT(test_array, pred_array, t_mcc, godag):
#     goterms = test_array.columns
#     mcc = {}
#     pred_log = (pred_array >= t_mcc)
#     for gid in goterms:
#         test_g = test_array.loc[:, gid]
#         pred_g = pred_array.loc[:, gid]
#         new_test_g = test_g[(test_g == 0) | (test_g == 1)]
#         new_pred_g = pred_g[(test_g == 0) | (test_g == 1)]
#         new_pred_log = pred_log.loc[:, gid][(test_g == 0) | (test_g == 1)]
#         # if len(new_test_g) != 0:
#         #     fpr, tpr, threaholds = roc_curve(new_test_g, new_pred_g)
#         #     tpr_vs_fpr.loc[gid, :] = interp(mean_fpr, fpr, tpr)
#         #
#         #     auc_go[gid] = auc(fpr, tpr)
#
#         got = gid[0:2] + ':' + gid[2:]
#         offsping_propogated = prop_go_terms_offsprings([got], godag)
#         for goff in offsping_propogated:
#             goff = goff[0:2] + goff[3:]
#             if goff in goterms:
#                 tmp = pred_log.loc[:, goff][(test_g == 0) | (test_g == 1)]
#                 new_pred_log |= tmp
#
#         new_pred_g[new_pred_log] = 1
#         new_pred_g[-new_pred_log] = 0
#         mcc[gid] = matthews_corrcoef(new_test_g, new_pred_g)
#
#     mcc_r = pd.DataFrame.from_dict(mcc, orient='index')
#     mcc_r.columns = [t_mcc]
#     return mcc_r


# def calculate_max_mcc(all_test, all_pred, threaholds):
#     max_mcc = 0
#     for t in threaholds:
#         all_pred_b = []
#         for i in xrange(len(all_pred)):
#             if all_pred[i] < t:
#                 all_pred_b.append(-1)
#             else:
#                 all_pred_b.append(1)
#         mcc = matthews_corrcoef(all_test, all_pred_b)
#         if mcc > max_mcc:
#             max_mcc = mcc
#     return max_mcc


# ##########################################################################################################
# # Save results
# ########################################################################################################
# def add_pred_results(all_proteins_pred, pids, test, pred, gid):
#     for pid, t, p in zip(pids, test, pred):
#         if pid in all_proteins_pred.keys():
#             all_proteins_pred[pid].loc['test', gid] = t
#             all_proteins_pred[pid].loc['pred', gid] = p
#         else:
#             all_proteins_pred[pid] = pd.DataFrame(data=0, index=['test', 'pred'], columns=[gid])
#             all_proteins_pred[pid].loc['test', gid] = t
#             all_proteins_pred[pid].loc['pred', gid] = p
#     return all_proteins_pred
#
#
# def parse_results(filename):
#     all_test = []
#     all_pred = []
#     all_ids = []
#     with open(filename, 'r') as f:
#         for line in f:
#             tmp = line.split('\t')
#             all_test.append(int(float(tmp[0])))
#             all_pred.append(float(tmp[1]))
#             all_ids.append(tmp[2])
#     return all_test, all_pred, all_ids
#
# def present_results(nnet):
#     logging.info('> [RESULTS] Calculating presicion, recall(sensitivity), specificity ...')
#     output_path = nnet['output_path']
#     gofilename = nnet['gofilename']
#     rocfilename = nnet['rocfilename']
#     mccfilename = nnet['mccfilename']
#     prfilename_gene = nnet['prfilename_gene']
#     prfilename_go = nnet['prfilename_go']
#
#     with open(gofilename, 'r') as f:
#         file_list = f.readlines()
#
#     cv_num = 2  # nnet['cv']
#     mean_tpr = 0.0
#     mean_fpr = np.linspace(0, 1, 100)
#     mean_pre = [0] * 100
#     mean_rec = [0] * 100
#     all_mcc = {}
#     all_proteins_pred = {}
#     for f in file_list:
#         f = f[:-1]
#         goid = f.split('.')[0]
#         all_test = []
#         all_pred = []
#         all_pids = []
#
#         for cv in xrange(cv_num):
#             tmp_filename = goid + '_' + str(cv) + '.txt'
#             test, pred, pids = parse_results(join(output_path, tmp_filename))
#             all_test.extend(test)
#             all_pred.extend(pred)
#             all_pids.extend(pids)
#             all_proteins_pred = add_pred_results(all_proteins_pred, pids, test, pred, goid)
#         fpr, tpr, threaholds = roc_curve(all_test, all_pred)
#         pre, rec, f1 = prf_curve_single(all_test, all_pred)
#         mean_tpr += interp(mean_fpr, fpr, tpr)
#         mean_tpr[0] = 0.0
#         mean_pre = [pre[i] + mean_pre[i] for i in xrange(100)]
#         mean_rec = [rec[i] + mean_rec[i] for i in xrange(100)]
#         all_mcc[goid] = calculate_max_mcc(all_test, all_pred, threaholds)
#     mean_tpr /= len(file_list)
#     mean_pre = np.array(mean_pre) / len(file_list)
#     mean_rec = np.array(mean_rec) / len(file_list)
#     mean_tpr[-1] = 1.0
#     '''
#     Save roc and prc results to file
#     '''
#     with open(join(output_path, rocfilename), 'w') as f:
#         for tpr, fpr in zip(mean_tpr, mean_fpr):
#             f.write('%f\t%f\n' % (tpr, fpr))
#
#     with open(join(output_path, mccfilename), 'w') as f:
#         for key, mcc in all_mcc.iteritems():
#             f.write('%s\t%f\n' % (key, mcc))
#
#     with open(join(output_path, prfilename_go), 'w') as f:
#         for pre, rec in zip(mean_pre, mean_rec):
#             f.write('%s\t%f\n' % (pre, rec))
#
#     '''
#     Calculate precision and recall performance in gene based maner
#     '''
#     mean_pre_gene = [0] * 100
#     mean_rec_gene = [0] * 100
#     for key, value in all_proteins_pred.iteritems():
#         pre, rec, f1 = prf_curve_single(value.loc['test'], value.loc['pred'])
#         mean_pre_gene = [pre[i] + mean_pre_gene[i] for i in xrange(100)]
#         mean_rec_gene = [rec[i] + mean_rec_gene[i] for i in xrange(100)]
#     mean_pre_gene = [mean_pre_gene[i] / len(all_proteins_pred.keys()) for i in xrange(100)]
#     mean_rec_gene = [mean_rec_gene[i] / len(all_proteins_pred.keys()) for i in xrange(100)]
#     with open(join(output_path, prfilename_gene), 'w') as f:
#         for pre, rec in zip(mean_pre_gene, mean_rec_gene):
#             f.write('%s\t%f\n' % (pre, rec))


