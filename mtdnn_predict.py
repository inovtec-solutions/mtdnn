#!/home/rui/anaconda2/bin/python

from multitask import MultiTaskDNN
from lasagne.nonlinearities import rectify, softmax, linear, sigmoid
import getopt
from os import makedirs
from utils import *
from os.path import exists, join
import sys
import logging
import pickle
import numpy as np
import pandas as pd

def xrange(x):
    return iter(range(x))

def main(argv):
    # logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    try:
        opts, args = getopt.getopt(argv, "hb:m:i:l:", ["branch_term=", "mode=", "inputfile=", "logfile="])
    except getopt.GetoptError:
        print ('mtdnn_predict.py -b <branch_term> -m <mode> -i <inputfile> -l <logfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print ('mtdnn_predict.py -b <branch_term> -m <mode> -i <inputfile> -l <logfile>')
            sys.exit()
        elif opt in ("-b", "--branch_term"):
            branch_term = arg
        elif opt in ("-m", "--mode"):
            mode = arg
        elif opt in ("-i", "--input"):
            all_data_x_fn = arg
        elif opt in ("-l", "--logfile"):
            log_gfile = arg
    logging.basicConfig(filename=log_gfile, format='%(asctime)s %(message)s', level=logging.INFO)
    configs_fn = 'configs/' + branch_term[0:2] + branch_term[3:] + '_mtdnn_final.dict'
    with smart_open(configs_fn, 'rb') as fp:
        configs = pickle.load(fp)
    input_width = configs['input_width']
    shared_input_depth = configs['shared_input_depth']
    spec_input_depth = configs['spec_input_depth']
    # exp_type = configs['exp_type']
    dropout = configs['dropout']
    shared_hidden_number = configs['shared_hidden_number']
    spec_hidden_number = configs['spec_hidden_number']
    output_number = configs['output_number']
    nonlinearity_hidden = rectify
    if mode == 'train':
        nonlinearity_output = softmax
    elif mode == 'predict':
        nonlinearity_output = sigmoid
    batch_size = configs['batch_size']
    num_task = configs['num_task']
    share_num_epochs = configs['share_num_epochs']
    spec_num_epochs = configs['spec_num_epochs']
    lrate = configs['lrate']
    momentum = configs['momentum']
    l1_lambda = configs['l1_lambda']
    l2_lambda = configs['l2_lambda']
    category = configs['category']
    output_path = configs['output_path']
    if not exists(output_path):
        makedirs(output_path)

    predict_fn = join(output_path, branch_term[0:2] + branch_term[3:] + '_final.predict')
    # metrics_fn = join(output_path, branch_term[0:2] + branch_term[3:] + '_' + category + '.metrics')
    if 'model_path' in configs.keys():
        # d = dirname(configs['model_path'])
        model_path = configs['model_path']
        if not exists(model_path):
            makedirs(model_path)
    else:
        logging.info('[ERROR] ''model_path'' has to be specified!')
        sys.exit(2)

    if mode == 'train':
        logging.info('[MTDNN][INIT] Initializing ...')
        random_state = np.random.RandomState(0)

        dfp = MultiTaskDNN(branch_term, category, input_width=input_width, shared_input_depth=shared_input_depth,
                           spec_input_depth=spec_input_depth, dropout=dropout, mode='train',
                           shared_hidden_number=shared_hidden_number,
                           spec_hidden_number=spec_hidden_number, num_task=num_task, output_number=output_number,
                           nonlinearity_hidden=nonlinearity_hidden, nonlinearity_output=nonlinearity_output,
                           batch_size=batch_size, share_num_epochs=share_num_epochs, spec_num_epochs=spec_num_epochs,
                           rng=random_state, lrate_shared=lrate, lrate_spec=lrate, momentum=momentum,
                           optimizer='adam',
                           model_file=join(model_path + 'mtdnn.model'), l1_lambda=l1_lambda, l2_lambda=l2_lambda)
        logging.info('[MTDNN][FIT] fitting ...')
        dfp.fit_final()
        del dfp
        nonlinearity_output = sigmoid
        random_state = np.random.RandomState(0)
        dfp = MultiTaskDNN(branch_term, category, input_width=input_width, shared_input_depth=shared_input_depth,
                           spec_input_depth=spec_input_depth, dropout=dropout, mode='predict',
                           shared_hidden_number=shared_hidden_number,
                           spec_hidden_number=spec_hidden_number, num_task=num_task, output_number=output_number,
                           nonlinearity_hidden=nonlinearity_hidden, nonlinearity_output=nonlinearity_output,
                           batch_size=batch_size, share_num_epochs=share_num_epochs, spec_num_epochs=spec_num_epochs,
                           rng=random_state, lrate_shared=lrate, lrate_spec=lrate, momentum=momentum,
                           optimizer='adam',
                           model_file=join(model_path + 'mtdnn.model'), l1_lambda=l1_lambda, l2_lambda=l2_lambda)

    elif mode == 'predict':
        logging.info('[MTDNN][LOAD] Loading model')
        random_state = np.random.RandomState(0)
        dfp = MultiTaskDNN(branch_term, category, input_width=input_width, shared_input_depth=shared_input_depth,
                           spec_input_depth=spec_input_depth, dropout=dropout, mode='predict',
                           shared_hidden_number=shared_hidden_number,
                           spec_hidden_number=spec_hidden_number, num_task=num_task, output_number=output_number,
                           nonlinearity_hidden=nonlinearity_hidden, nonlinearity_output=nonlinearity_output,
                           batch_size=batch_size, share_num_epochs=share_num_epochs, spec_num_epochs=spec_num_epochs,
                           rng=random_state, lrate_shared=lrate, lrate_spec=lrate, momentum=momentum,
                           optimizer='adam',
                           model_file=join(model_path + 'mtdnn.model'), l1_lambda=l1_lambda, l2_lambda=l2_lambda)

    logging.info('[MTDNN][PREDICT] predicting ...')
    # all_data_x_fn = 'data/all_features_cafa3.csv'
    all_data_x = pd.read_csv(all_data_x_fn, sep='\t', header=0, index_col=0)
    all_proteins = [p.replace('"', '') for p in all_data_x.index]
    all_data_x.index = all_proteins
    test_fn = 'data/train_sets/' + branch_term[0:2] + branch_term[3:] + '_test.csv'
    test_y = pd.read_csv(test_fn, sep='\t', header=0, index_col=0)
    dfp.set_weights()
    pred_y = dfp.predict(all_data_x)
    all_pred_y = pd.DataFrame(np.array(pred_y).transpose(), index=all_data_x.index, columns=test_y.columns)

    save_prediction(predict_fn, all_pred_y)
    logging.info('[MTDNN][PREDICT] predicting finished!')

if __name__ == "__main__":
    main(sys.argv[1:])
